import 'dart:io';

import 'package:image_picker/image_picker.dart';

import '../exceptions/exceptions.dart';

class ImageService {
  static Future<File> getImage() async {
    final ImagePicker picker = ImagePicker();
    try {
      final XFile? image =
          await picker.pickImage(source: ImageSource.gallery, imageQuality: 15);
      if (image != null) {
        return File(image.path);
      } else {
        throw ImageException('Failed to load image');
      }
    } catch (e) {
      throw ImageException('Failed to load image');
    }
  }

  static Future<File> getImageComp() async {
    final ImagePicker picker = ImagePicker();
    try {
      final XFile? image =
          await picker.pickImage(source: ImageSource.gallery, imageQuality: 15);
      if (image != null) {
        return File(image.path);
      } else {
        throw ImageException('Failed to load image');
      }
    } catch (e) {
      throw ImageException('Failed to load image');
    }
  }

  static Future<List<File>> getImages() async {
    final ImagePicker picker = ImagePicker();
    try {
      final List<XFile>? images = await picker.pickMultiImage(imageQuality: 15);

      if (images != null) {
        return images.map((XFile image) => File(image.path)).toList();
      } else {
        throw ImageException('Failed to load images');
      }
    } catch (e) {
      throw ImageException('Failed to load images');
    }
  }
}
