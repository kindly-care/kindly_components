import 'package:another_flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';

void successSnackbar(BuildContext context, String message,
    {Duration duration = const Duration(seconds: 5)}) {
  FlushbarHelper.createSuccess(
          title: 'Success', message: message, duration: duration)
      .show(context);
}

void errorSnackbar(BuildContext context, String message,
    {Duration duration = const Duration(seconds: 5)}) {
  FlushbarHelper.createError(
    title: 'Error',
    message: message,
    duration: duration,
  ).show(context);
}

void retrySnackbar(BuildContext context, String message,
    {required VoidCallback onRetry,
    Duration duration = const Duration(seconds: 8)}) {
  FlushbarHelper.createAction(
    title: 'Error',
    message: message,
    duration: duration,
    button: TextButton(onPressed: onRetry, child: const Text('Retry')),
  ).show(context);
}
