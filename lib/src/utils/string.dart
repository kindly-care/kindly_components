import 'package:intl/intl.dart';

/// takes number of item plus item name eg 2, Apple
String plurals(num? number, String item) {
  final num value = number ?? 0;
  return Intl.plural(
    value,
    zero: '$value $item' 's',
    one: '$value $item',
    other: '$value $item' 's',
  );
}

List<String> getShortDaysFromLong(List<String> longDays) {
  return longDays.isNotEmpty
      ? longDays.map((String day) => day.substring(0, 2)).toList()
      : <String>[];
}

List<String> getLongDaysFromShort(List<String> shortDays) {
  final List<String> longDays = <String>[];

  for (final String day in shortDays) {
    if (day == 'Mon') {
      longDays.add('Monday');
    }
    if (day == 'Tue') {
      longDays.add('Tuesday');
    }
    if (day == 'Wed') {
      longDays.add('Wednesday');
    }
    if (day == 'Thu') {
      longDays.add('Thursday');
    }
    if (day == 'Fri') {
      longDays.add('Friday');
    }
    if (day == 'Sat') {
      longDays.add('Saturday');
    }
    if (day == 'Sun') {
      longDays.add('Sunday');
    }
  }

  return longDays;
}
