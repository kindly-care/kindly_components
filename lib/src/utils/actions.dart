// ignore_for_file: deprecated_member_use

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:url_launcher/url_launcher.dart' as launcher;

import '../models/care_provider/care_provider.dart';
import '../toast.dart';

/// Opens device's phone app to make a phone call. Shows a toast if phone app can't be opened
Future<void> onMakeCall(String phoneNumber) async {
  final bool numberIsValid = num.tryParse(phoneNumber) != null;

  if (numberIsValid) {
    if (await launcher.canLaunch('tel:$phoneNumber')) {
      await launcher.launch('tel:$phoneNumber');
    } else {
      showToast(message: 'Failed to make call');
    }
  } else {
    showToast(message: 'Failed to make call');
  }
}

/// Opens device's email app to send an email message. Shows a toast if email app can't be opened
Future<void> onEmail(String emailAddress) async {
  final bool isEmailValid =
      emailAddress.isNotEmpty && emailAddress.contains('@');
  if (isEmailValid) {
    if (await launcher.canLaunch('mailto:$emailAddress')) {
      await launcher.launch('mailto:$emailAddress');
    } else {
      showToast(message: 'Email initialization failed.');
    }
  } else {
    showToast(message: 'Email initialization failed.');
  }
}

/// Returns availability status of a Care Provider
String getAvailability(CareProvider careProvider) {
  final bool isAvailable = careProvider.availability.values.any((String val) =>
      val == 'Available' || val == 'Available Day' || val == 'Available Night');
  if (isAvailable) {
    return 'Available';
  } else {
    return 'Unavailable';
  }
}

/// Displays a modal bottom sheet with a Cupertino inspired DateTime picker
void showDateTimeSelector(
  BuildContext context, {
  DateTime? minDate,
  DateTime? maxDate,
  DateFormat? format,
  DateTime? initialDate,
  required String title,
  required TextEditingController controller,
  required Function(DateTime date) onDateChanged,
  CupertinoDatePickerMode mode = CupertinoDatePickerMode.dateAndTime,
}) {
  final TextTheme theme = Theme.of(context).textTheme;
  DateTime? selectedDate = initialDate;
  showCupertinoModalPopup<void>(
    context: context,
    builder: (_) {
      return Container(
        height: 48.0.h,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12.0),
            topRight: Radius.circular(12.0),
          ),
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    TextButton(
                      child: Text(
                        'Cancel',
                        style: theme.bodyLarge?.copyWith(
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      onPressed: () => Navigator.pop(context),
                    ),
                    Text(title, style: theme.bodyLarge),
                    TextButton(
                      child: Text(
                        'Ok',
                        style: theme.bodyLarge?.copyWith(
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      onPressed: () {
                        if (selectedDate != null) {
                          onDateChanged(selectedDate!);
                          if (format != null) {
                            controller.text = format.format(selectedDate!);
                          } else {
                            controller.text = DateFormat.yMMMMd('en_GB')
                                .format(selectedDate!);
                          }
                        }

                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),
            ),
            Divider(height: 1.0.h),
            Expanded(
              flex: 6,
              child: CupertinoDatePicker(
                mode: mode,
                use24hFormat: true,
                maximumDate: maxDate,
                minimumDate: minDate,
                dateOrder: DatePickerDateOrder.dmy,
                initialDateTime: initialDate ?? DateTime.now(),
                onDateTimeChanged: (DateTime date) => selectedDate = date,
              ),
            ),
          ],
        ),
      );
    },
  );
}
