import '../models/time_log/time_log.dart';

/// returns both hours and minutes from a list of timelogs
String getHoursMinutesFromTimeLogs(List<TimeLog> timelogs) {
  final int totalMinutes = timelogs.fold<int>(0, (int prev, TimeLog timeLog) {
    final int minutes =
        timeLog.finishTime!.difference(timeLog.startTime!).inMinutes;
    return prev + minutes;
  });

  final int hours = totalMinutes ~/ 60;
  final int minutes = totalMinutes % 60;

  if (hours < 10 && minutes < 10) {
    return '0$hours:0${minutes.toString()}';
  } else if (hours < 10 && minutes > 10) {
    return '0$hours:${minutes.toString()}';
  } else if (hours > 10 && minutes < 10) {
    return '$hours:0${minutes.toString()}';
  } else {
    return '${hours.toString()}:${minutes.toString()}';
  }
}

/// returns hours from a list of timelogs
String getHoursFromTimeLogs(List<TimeLog> timelogs) {
  final int totalMinutes = timelogs.fold<int>(0, (int prev, TimeLog timeLog) {
    final int minutes =
        timeLog.finishTime!.difference(timeLog.startTime!).inMinutes;
    return prev + minutes;
  });

  final int hours = totalMinutes ~/ 60;

  return hours.toString();
}
