import 'package:permission_handler/permission_handler.dart';

abstract class PermissionUtil {
  static Future<void> requestPermission() async {
    await <Permission>[
      Permission.location,
      Permission.storage,
      Permission.camera,
    ].request();
  }
}
