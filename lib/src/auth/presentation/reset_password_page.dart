import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../extensions/extensions.dart';
import '../../snackbar.dart';
import '../../utils/dialogs.dart';
import '../../widgets/widgets.dart';
import '../bloc/auth_bloc.dart';

class ResetPasswordPage extends StatefulWidget {
  const ResetPasswordPage({super.key});

  @override
  State<ResetPasswordPage> createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  late GlobalKey<FormState> _formKey;
  late TextEditingController _emailController;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    _emailController = TextEditingController();
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  void _onSendResetPasswordLink() {
    final bool isValid = _formKey.currentState?.validate() ?? false;
    if (isValid) {
      context.read<AuthBloc>().add(ResetPassword(email: _emailController.text));
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Reset Password'),
      ),
      body: BlocListener<AuthBloc, AuthState>(
        listener: (BuildContext context, AuthState state) {
          if (state is ResetPasswordSuccess) {
            _showResetPasswordDialog(state.message);
          } else if (state is ResetPasswordError) {
            errorSnackbar(context, state.message);
          }
        },
        child: Form(
          key: _formKey,
          child: ListView(
            padding: EdgeInsets.all(3.0.w),
            physics: const BouncingScrollPhysics(),
            children: <Widget>[
              SizedBox(height: 1.5.h),
              Text('Forgot your password?', style: theme.titleLarge),
              SizedBox(height: 3.2.h),
              Text(
                "Enter your email and we'll send you a link to reset your password.",
                style: theme.bodyLarge
                    ?.copyWith(fontSize: 18.5.sp, fontWeight: FontWeight.w500),
              ),
              SizedBox(height: 3.5.h),
              MaterialTextField(
                controller: _emailController,
                hintText: 'Enter email address',
                onChanged: (_) => setState(() {}),
                textInputType: TextInputType.emailAddress,
                onFieldSubmitted: (_) => _onSendResetPasswordLink(),
                validator: (String? email) => email.simpleValidateEmail,
              ),
              SizedBox(height: 11.5.h),
              SizedBox(
                width: double.infinity,
                child: Builder(builder: (BuildContext context) {
                  final bool isLoading =
                      context.select((AuthBloc bloc) => bloc.state)
                          is ResetPasswordInProgress;
                  return ActionButton(
                    title: 'Reset Password',
                    isLoading: isLoading,
                    onPressed: _onSendResetPasswordLink,
                    enabled: _emailController.text.simpleValidateEmail == null,
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showResetPasswordDialog(String message) {
    showOneButtonDialog(
      context,
      content: message,
      barrierDismissible: false,
      title: 'Reset Password',
      buttonText: 'Exit',
      onPressed: () {
        Navigator.pop(context);
        SystemNavigator.pop();
      },
    );
  }
}
