import 'package:entry/entry.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../extensions/extensions.dart';
import '../../models/models.dart';
import '../../snackbar.dart';
import '../../widgets/widgets.dart';
import '../bloc/auth_bloc.dart';
import 'create_account_page.dart';
import 'reset_password_page.dart';

class LoginPage extends StatefulWidget {
  final AuthDetails? details;
  final Function(AppUser) onAuthSuccess;

  const LoginPage({super.key, required this.onAuthSuccess, this.details});

  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<LoginPage> {
  late AuthBloc _authBloc;
  bool _hidePassword = true;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    _authBloc = context.read<AuthBloc>();
    _emailController = TextEditingController(text: widget.details?.email);
    _passwordController = TextEditingController(text: widget.details?.password);
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onCreateAccountTap() {
    Navigator.push(
        context, CupertinoPageRoute(builder: (_) => const CreateAccountPage()));
  }

  Future<void> _onEmailLogin() async {
    final bool isValid = _formKey.currentState?.validate() ?? false;
    if (isValid) {
      final AuthDetails details = AuthDetails(
        name: widget.details?.name,
        phone: widget.details?.phone,
        email: _emailController.text.trim(),
        password: _passwordController.text.trim(),
      );
      _authBloc.add(SignIn(details: details));
    }
  }

  void _onForgotPasswordTap() {
    Navigator.push(
        context, CupertinoPageRoute(builder: (_) => const ResetPasswordPage()));
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(0.0),
        child: AppBar(
          elevation: 0.0,
          backgroundColor: Theme.of(context).backgroundColor,
          automaticallyImplyLeading: false,
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Theme.of(context).backgroundColor,
            statusBarIconBrightness: Brightness.dark,
          ),
        ),
      ),
      body: BlocListener<AuthBloc, AuthState>(
        listener: (BuildContext context, AuthState state) {
          if (state is SigningInError) {
            errorSnackbar(context, state.message);
          } else if (state is Authenticated) {
            widget.onAuthSuccess(state.user);
          }
        },
        child: Entry(
          opacity: 0.3,
          yOffset: 100,
          duration: const Duration(milliseconds: 500),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 25.0.h),
                    child: Center(
                      child: Text(
                        'Kindly Care',
                        style: TextStyle(
                          fontFamily: 'Oleo',
                          fontSize: 31.0.sp,
                          fontWeight: FontWeight.w600,
                          color: Colors.black.withOpacity(0.58),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 14.5.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4.0.w),
                    child: MaterialTextField(
                      labelText: 'Email',
                      controller: _emailController,
                      hintText: 'Enter email address',
                      onChanged: (_) => setState(() {}),
                      textInputType: TextInputType.emailAddress,
                      validator: (String? email) => email.simpleValidateEmail,
                    ),
                  ),
                  SizedBox(height: 2.0.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4.0.w),
                    child: MaterialTextField(
                      maxLines: 1,
                      minLines: null,
                      labelText: 'Password',
                      hintText: 'Enter password',
                      obscureText: _hidePassword,
                      controller: _passwordController,
                      onChanged: (_) => setState(() {}),
                      textInputAction: TextInputAction.go,
                      textInputType: TextInputType.visiblePassword,
                      onFieldSubmitted: (_) => _onEmailLogin(),
                      validator: (String? email) => email.simpleValidate,
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() => _hidePassword = !_hidePassword);
                        },
                        icon: Icon(
                          _hidePassword
                              ? Icons.visibility_outlined
                              : Icons.visibility_off_outlined,
                          // size: 3.8.h,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 2.5.h),
                  Builder(builder: (BuildContext context) {
                    final bool isLoading = context
                        .select((AuthBloc bloc) => bloc.state) is SigningIn;

                    return CupertinoButton.filled(
                      onPressed: _onEmailLogin,
                      padding: EdgeInsets.symmetric(horizontal: 35.5.w),
                      child: isLoading
                          ? const LoadingWidgetButton()
                          : const Text('Sign in'),
                    );
                  }),
                  SizedBox(height: 5.0.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      TextButton(
                        onPressed: _onCreateAccountTap,
                        child: Text(
                          'Create Account',
                          style: theme.bodyLarge
                              ?.copyWith(color: Theme.of(context).primaryColor),
                        ),
                      ),
                      TextButton(
                        onPressed: _onForgotPasswordTap,
                        child: Text(
                          'Forgot Password?',
                          style: theme.bodyLarge
                              ?.copyWith(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
