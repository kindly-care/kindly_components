import 'package:entry/entry.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../extensions/extensions.dart';
import '../../models/models.dart';
import '../../snackbar.dart';
import '../../utils/dialogs.dart';
import '../../widgets/widgets.dart';
import '../bloc/auth_bloc.dart';

class CreateAccountPage extends StatefulWidget {
  const CreateAccountPage({super.key});

  @override
  CreateAccountPageState createState() => CreateAccountPageState();
}

class CreateAccountPageState extends State<CreateAccountPage> {
  bool _hidePassword = true;
  late FocusNode _confirmNode;
  bool _hideConfirmPassword = true;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _nameController;
  late TextEditingController _phoneController;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late TextEditingController _confirmController;

  @override
  void initState() {
    super.initState();
    _confirmNode = FocusNode();
    _formKey = GlobalKey<FormState>();
    _nameController = TextEditingController();
    _phoneController = TextEditingController();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _confirmController = TextEditingController();
  }

  @override
  void dispose() {
    _confirmNode.dispose();
    _nameController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmController.dispose();
    super.dispose();
  }

  Future<void> _onEmailSignup() async {
    final bool isValid = _formKey.currentState?.validate() ?? false;
    if (isValid) {
      final AuthDetails details = AuthDetails(
        phone: _phoneController.text.trim(),
        email: _emailController.text.trim(),
        password: _passwordController.text.trim(),
        name: _nameController.text.capitalizeFirstofEach,
      );

      context.read<AuthBloc>().add(SignUp(details: details));
    }
  }

  String? _confirmPasswordValidator(String? text) {
    if (text!.isEmpty) {
      return 'Field required';
    } else if (text != _passwordController.text) {
      return 'Passwords do not match';
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: const Text('Create Account'),
      ),
      body: BlocListener<AuthBloc, AuthState>(
        listener: (BuildContext context, AuthState state) {
          if (state is SigningUpSuccess) {
            _showVerificationSendDialog(state.message);
          } else if (state is SigningUpError) {
            errorSnackbar(context, state.message);
          }
        },
        child: Entry(
          opacity: 0.3,
          yOffset: 100,
          duration: const Duration(milliseconds: 500),
          child: SingleChildScrollView(
            padding: EdgeInsets.all(3.0.w),
            physics: const BouncingScrollPhysics(),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const TextHeader('Details'),
                  SizedBox(height: 2.0.h),
                  MaterialTextField(
                    labelText: 'Name',
                    hintText: 'Enter full name',
                    controller: _nameController,
                    onChanged: (_) => setState(() {}),
                    validator: (String? val) => val.simpleValidate,
                  ),
                  SizedBox(height: 2.5.h),
                  MaterialTextField(
                    labelText: 'Phone',
                    controller: _phoneController,
                    hintText: 'Enter phone number',
                    onChanged: (_) => setState(() {}),
                    textInputType: TextInputType.phone,
                    validator: (String? val) => val.simpleValidateNumber,
                  ),
                  SizedBox(height: 2.5.h),
                  MaterialTextField(
                    labelText: 'Email',
                    controller: _emailController,
                    hintText: 'Enter email address',
                    onChanged: (_) => setState(() {}),
                    textInputType: TextInputType.emailAddress,
                    validator: (String? email) => email.simpleValidateEmail,
                  ),
                  SizedBox(height: 3.5.h),
                  const TextHeader('Passwords'),
                  SizedBox(height: 2.0.h),
                  MaterialTextField(
                    maxLines: 1,
                    minLines: null,
                    labelText: 'Password',
                    hintText: 'Enter password',
                    obscureText: _hidePassword,
                    controller: _passwordController,
                    onChanged: (_) => setState(() {}),
                    textInputAction: TextInputAction.next,
                    textInputType: TextInputType.visiblePassword,
                    validator: (String? email) => email.simpleValidatePassword,
                    onFieldSubmitted: (_) => _confirmNode.requestFocus(),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() => _hidePassword = !_hidePassword);
                      },
                      icon: Icon(
                        _hidePassword
                            ? Icons.visibility_outlined
                            : Icons.visibility_off_outlined,
                        // size: 3.8.h,
                      ),
                    ),
                  ),
                  SizedBox(height: 2.5.h),
                  MaterialTextField(
                    maxLines: 1,
                    minLines: null,
                    focusNode: _confirmNode,
                    labelText: 'Confirm Password',
                    hintText: 'Enter password',
                    controller: _confirmController,
                    obscureText: _hideConfirmPassword,
                    onChanged: (_) => setState(() {}),
                    textInputAction: TextInputAction.go,
                    validator: _confirmPasswordValidator,
                    onFieldSubmitted: (_) => _onEmailSignup(),
                    textInputType: TextInputType.visiblePassword,
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(
                            () => _hideConfirmPassword = !_hideConfirmPassword);
                      },
                      icon: Icon(
                        _hideConfirmPassword
                            ? Icons.visibility_outlined
                            : Icons.visibility_off_outlined,
                      ),
                    ),
                  ),
                  SizedBox(height: 8.0.h),
                  SizedBox(
                    width: double.infinity,
                    child: Builder(builder: (BuildContext context) {
                      final bool isLoading =
                          context.select((AuthBloc bloc) => bloc.state)
                              is SigningUpInProgress;
                      return CupertinoButton.filled(
                        onPressed: _onEmailSignup,
                        child: isLoading
                            ? const LoadingWidgetButton()
                            : const Text('Sign up'),
                      );
                    }),
                  ),
                  SizedBox(height: 4.0.h),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _showVerificationSendDialog(String message) {
    showOneButtonDialog(
      context,
      content: message,
      title: 'Verification',
      buttonText: 'Ok',
      barrierDismissible: false,
      onPressed: () {
        Navigator.pop(context);
        SystemNavigator.pop();
      },
    );
  }
}
