import '../../models/models.dart';

abstract class AuthRepository {
  /// Register user using email and password. Throws [AuthException]
  Future<void> registerWithEmail(AuthDetails details);

  /// Sign in user with email and password. Throws [AuthException]
  Future<void> signInWithEmail(AuthDetails details);

  /// returns a Stream of [AppUser]. Throws [FetchDataException].
  Stream<AppUser?> fetchCurrentUser();

  /// takes in a String [uid] and returns [AppUser]. Throws [FetchDataException].
  Future<AppUser?> fetchUserById(String uid);

  /// takes in a String [email] and returns [AppUser]. Throws [FetchDataException].
  Future<AppUser?> fetchUserByEmail(String email);

  /// saves [AuthDetails] to cache. Throws [UpdateDataException].
  Future<void> saveAuthDetails(AuthDetails auth);

  /// fetches [AuthDetails] from cache. Throws [FetchDataException].
  Future<AuthDetails?> fetchAuthDetails();

  /// Resets user password. Takes String [email]. Throws [AuthException]
  Future<void> resetPassword(String email);

  /// signs out current user.
  Future<void> signOut();

  /// deletes user account. Throws [UpdateDataException].
  Future<void> deleteAccount();
}
