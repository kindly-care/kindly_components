import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../exceptions/exceptions.dart';
import '../../models/models.dart';
import '../repository/auth_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository _authRepository;
  late AppUser user;

  AuthBloc({required AuthRepository authRepository})
      : _authRepository = authRepository,
        super(AuthInitial()) {
    on<StartApp>(_onStartApp);
    on<SignIn>(_onSignIn);
    on<SignUp>(_onSignUp);
    on<Logout>(_onLogout);
    on<ResetPassword>(_onResetPassword);
    on<DeleteAccount>(_onDeleteAccount);
  }

  Future<void> _onStartApp(StartApp event, Emitter<AuthState> emit) async {
    final AuthDetails? details = await _authRepository.fetchAuthDetails();

    await emit.forEach<AppUser?>(
      _authRepository.fetchCurrentUser(),
      onData: (AppUser? appUser) {
        if (appUser != null) {
          user = appUser;

          return Authenticated(user: appUser);
        } else {
          return Unauthenticated(details: details);
        }
      },
      onError: (_, __) => const AuthError(
        message: 'An error occurred. Please try again.',
      ),
    );
  }

  Future<void> _onSignIn(SignIn event, Emitter<AuthState> emit) async {
    emit(SigningIn());
    try {
      await _authRepository.signInWithEmail(event.details);
      add(StartApp());
    } on AuthException catch (e) {
      emit(SigningInError(message: e.toString()));
    }
  }

  Future<void> _onSignUp(SignUp event, Emitter<AuthState> emit) async {
    emit(SigningUpInProgress());

    try {
      await _authRepository.registerWithEmail(event.details);
      emit(const SigningUpSuccess(
          message:
              "We've just send a verification link to your email inbox. You need to click on the link to verify your email address before you can sign into the app."));
    } on AuthException catch (e) {
      emit(SigningUpError(message: e.toString()));
    }
  }

  Future<void> _onLogout(Logout event, Emitter<AuthState> emit) async {
    await _authRepository.signOut();
    emit(const Unauthenticated());
  }

  Future<void> _onResetPassword(
      ResetPassword event, Emitter<AuthState> emit) async {
    emit(ResetPasswordInProgress());

    final String email = event.email;

    try {
      await _authRepository.resetPassword(email);
      emit(ResetPasswordSuccess(
        message:
            "We've just send an email to $email. Please check your email inbox to reset your password.",
      ));
    } on AuthException catch (e) {
      emit(ResetPasswordError(message: e.toString()));
    }
  }

  Future<void> _onDeleteAccount(
      DeleteAccount event, Emitter<AuthState> emit) async {
    await _authRepository.deleteAccount();
    await _authRepository.signOut();

    emit(const Unauthenticated());
  }
}
