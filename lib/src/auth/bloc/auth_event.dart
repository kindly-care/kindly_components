part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => <Object>[];
}

class StartApp extends AuthEvent {}

class SignIn extends AuthEvent {
  final AuthDetails details;
  const SignIn({required this.details});

  @override
  List<Object> get props => <Object>[details];
}

class SignUp extends AuthEvent {
  final AuthDetails details;
  const SignUp({required this.details});

  @override
  List<Object> get props => <Object>[details];
}

class Logout extends AuthEvent {}

class ResetPassword extends AuthEvent {
  final String email;
  const ResetPassword({required this.email});

  @override
  List<Object> get props => <Object>[email];
}

class DeleteAccount extends AuthEvent {}
