part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object?> get props => <Object?>[];
}

class AuthInitial extends AuthState {}

class Authenticated extends AuthState {
  final AppUser user;
  const Authenticated({required this.user});

  @override
  List<Object> get props => <Object>[user];
}

class Unauthenticated extends AuthState {
  final AuthDetails? details;
  const Unauthenticated({this.details});

  @override
  List<Object?> get props => <Object?>[details];
}

class AuthError extends AuthState {
  final String message;
  const AuthError({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class SigningIn extends AuthState {}

class SigningInError extends AuthState {
  final String message;
  const SigningInError({required this.message});

  @override
  List<Object?> get props => <Object?>[message];
}

class SigningUpInProgress extends AuthState {}

class SigningUpError extends AuthState {
  final String message;
  const SigningUpError({required this.message});

  @override
  List<Object?> get props => <Object?>[message];
}

class SigningUpSuccess extends AuthState {
  final String message;
  const SigningUpSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class ResetPasswordInProgress extends AuthState {}

class ResetPasswordSuccess extends AuthState {
  final String message;
  const ResetPasswordSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class ResetPasswordError extends AuthState {
  final String message;
  const ResetPasswordError({required this.message});

  @override
  List<Object> get props => <Object>[message];
}
