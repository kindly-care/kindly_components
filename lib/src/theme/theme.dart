import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class AppTheme {
  static TextTheme _buildTextTheme(TextTheme base) {
    const String fontName = 'ProDisplay';
    final Color color = Colors.black.withOpacity(0.65);

    return base.copyWith(
      displayLarge: base.displayLarge?.copyWith(
        color: color,
        fontSize: 57.0.sp,
        fontFamily: fontName,
      ),
      displayMedium: base.displayMedium?.copyWith(
        color: color,
        fontSize: 45.0.sp,
        fontFamily: fontName,
      ),
      displaySmall: base.displaySmall?.copyWith(
        color: color,
        fontSize: 36.0.sp,
        fontFamily: fontName,
      ),
      headlineLarge: base.headlineLarge?.copyWith(
        color: color,
        fontSize: 32.0.sp,
        fontFamily: fontName,
      ),
      headlineMedium: base.headlineMedium?.copyWith(
        color: color,
        fontSize: 28.0.sp,
        fontFamily: fontName,
      ),
      headlineSmall: base.headlineSmall?.copyWith(
        color: color,
        fontSize: 21.0.sp,
        fontFamily: fontName,
        fontWeight: FontWeight.w700,
      ),
      titleLarge: base.titleLarge?.copyWith(
        color: color,
        fontSize: 20.0.sp,
        fontFamily: fontName,
        fontWeight: FontWeight.w700,
      ),
      titleMedium: base.titleMedium?.copyWith(
        color: color,
        fontSize: 18.0.sp,
        fontFamily: fontName,
        fontWeight: FontWeight.w600,
      ),
      titleSmall: base.titleSmall?.copyWith(
        fontSize: 17.4.sp,
        fontFamily: fontName,
        fontWeight: FontWeight.w500,
        color: Colors.black.withOpacity(0.6),
      ),
      bodyLarge: base.bodyLarge?.copyWith(
        color: color,
        fontSize: 17.8.sp,
        fontFamily: fontName,
        fontWeight: FontWeight.w600,
      ),
      bodyMedium: base.bodyMedium?.copyWith(
        color: color,
        fontSize: 17.0.sp,
        fontFamily: fontName,
        fontWeight: FontWeight.w500,
      ),
      bodySmall: base.bodySmall?.copyWith(
        color: color,
        fontSize: 12.0.sp,
        fontFamily: fontName,
      ),
      labelLarge: base.labelLarge?.copyWith(
        color: color,
        fontSize: 16.0.sp,
        fontFamily: fontName,
      ),
      labelMedium: base.labelMedium?.copyWith(
        color: color,
        fontSize: 15.0.sp,
        fontFamily: fontName,
      ),
      labelSmall: base.labelSmall?.copyWith(
        color: color,
        fontSize: 11.0.sp,
        fontFamily: fontName,
      ),
    );
  }

  static ThemeData lightTheme() {
    final Color black = Colors.black.withOpacity(0.75);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.grey[100],
      statusBarIconBrightness: Brightness.dark,
    ));
    const Color primaryColor = Color(0xFF54D3C2);
    const Color secondaryColor = Color(0xFF54D3C2);
    final ColorScheme colorScheme = const ColorScheme.light().copyWith(
      primary: primaryColor,
      secondary: secondaryColor,
    );
    final ThemeData base = ThemeData.light();
    return base.copyWith(
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: colorScheme.onSurface,
        selectionColor: colorScheme.brightness == Brightness.light
            ? Colors.black.withOpacity(0.4)
            : Colors.white.withOpacity(0.4),
        selectionHandleColor: colorScheme.secondary,
      ),
      primaryColor: primaryColor,
      primaryColorLight: Colors.teal.shade50,
      // useMaterial3: true,
      indicatorColor: Colors.white,
      splashColor: Colors.white24,
      splashFactory: InkRipple.splashFactory,
      canvasColor: Colors.white,
      backgroundColor: const Color(0xFFF4F4F4),
      scaffoldBackgroundColor: const Color(0xFFF4F4F4),
      errorColor: CupertinoColors.destructiveRed,
      buttonTheme: ButtonThemeData(
        colorScheme: colorScheme,
        textTheme: ButtonTextTheme.primary,
      ),
      textTheme: _buildTextTheme(base.textTheme),
      primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
      appBarTheme: AppBarTheme(
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Color(0xFFF4F4F4),
          statusBarIconBrightness: Brightness.dark,
        ),
        color: const Color(0xFFF4F4F4),
        elevation: 0.2,
        centerTitle: true,
        titleTextStyle: _buildTextTheme(base.primaryTextTheme).titleLarge,
        iconTheme: IconThemeData(color: black),
        actionsIconTheme: IconThemeData(color: black),
      ),
      dialogTheme: DialogTheme(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        titleTextStyle: _buildTextTheme(base.textTheme).headlineMedium,
      ),
      platform: TargetPlatform.iOS,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      colorScheme: colorScheme.copyWith(secondary: secondaryColor),
    );
  }
}
