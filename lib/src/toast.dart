import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'enums/enums.dart';

void showToast({
  required String message,
  ToastGravity gravity = ToastGravity.CENTER,
  ToastBrightness brightness = ToastBrightness.dark,
}) {
  Fluttertoast.showToast(
    msg: message,
    gravity: gravity,
    backgroundColor:
        brightness == ToastBrightness.light ? Colors.white : Colors.black,
    textColor: brightness == ToastBrightness.light
        ? Colors.black.withOpacity(0.7)
        : Colors.white,
  );
}
