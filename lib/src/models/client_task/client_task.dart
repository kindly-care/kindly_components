import 'package:json_annotation/json_annotation.dart';

part 'client_task.g.dart';

@JsonSerializable()
class ClientTask {
  final String task;
  final bool isDone;
  final DateTime date;
  final String? nonCompletionReason;
  ClientTask({
    required this.task,
    required this.isDone,
    required this.date,
    this.nonCompletionReason,
  });

  ClientTask copyWith({
    String? task,
    bool? isDone,
    DateTime? date,
    String? nonCompletionReason,
  }) {
    return ClientTask(
      task: task ?? this.task,
      isDone: isDone ?? this.isDone,
      date: date ?? this.date,
      nonCompletionReason: nonCompletionReason ?? this.nonCompletionReason,
    );
  }

  factory ClientTask.fromJson(Map<String, dynamic> json) =>
      _$ClientTaskFromJson(json);

  Map<String, dynamic> toJson() => _$ClientTaskToJson(this);
}
