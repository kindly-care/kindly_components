// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client_task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClientTask _$ClientTaskFromJson(Map<String, dynamic> json) => ClientTask(
      task: json['task'] as String,
      isDone: json['isDone'] as bool,
      date: DateTime.parse(json['date'] as String),
      nonCompletionReason: json['nonCompletionReason'] as String?,
    );

Map<String, dynamic> _$ClientTaskToJson(ClientTask instance) =>
    <String, dynamic>{
      'task': instance.task,
      'isDone': instance.isDone,
      'date': instance.date.toIso8601String(),
      'nonCompletionReason': instance.nonCompletionReason,
    };
