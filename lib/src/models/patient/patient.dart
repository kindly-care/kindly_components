import 'dart:io';

import 'package:json_annotation/json_annotation.dart';

import '../../enums/enums.dart';
import '../address/address.dart';
import '../app_user/app_user.dart';
import '../assigned_care_provider/assigned_care_provider.dart';
import '../meal/meal.dart';
import '../records/blood_glucose_record/blood_glucose_record.dart';
import '../records/blood_pressure_record/blood_pressure_record.dart';
import '../records/temperature_record/temperature_record.dart';
import '../records/weight_record/weight_record.dart';

part 'patient.g.dart';

@JsonSerializable(explicitToJson: true)
class Patient extends AppUser {
  final String bio;
  final String mobility;
  final String maritalStatus;
  final List<String> nextOfKin;
  final List<String> allergies;
  final List<String> conditions;
  final Map<String, Meal> meals;
  final DateTime subscriptionEnds;
  final List<String> circleMembers;
  final Map<String, String> circle;
  final WeightRecord? lastWeightRecord;
  final BloodGlucoseRecord? lastBloodGlucoseRecord;
  final TemperatureRecord? lastTemperatureRecord;
  final BloodPressureRecord? lastBloodPressureRecord;
  final Map<String, AssignedCareProvider> assignedCareProviders;

  const Patient({
    String? email,
    required this.bio,
    required String uid,
    required this.circle,
    required String name,
    required String gender,
    required DateTime dob,
    required String phone,
    required this.mobility,
    required String avatar,
    this.lastWeightRecord,
    required this.allergies,
    required this.nextOfKin,
    required this.conditions,
    required Address address,
    this.lastBloodGlucoseRecord,
    this.lastTemperatureRecord,
    required this.circleMembers,
    required String streamToken,
    this.lastBloodPressureRecord,
    required this.maritalStatus,
    required DateTime joinedDate,
    required String messageToken,
    required this.subscriptionEnds,
    required List<String> contacts,
    required this.assignedCareProviders,
    this.meals = const <String, Meal>{},
    UserType userType = UserType.patient,
    File? localAvatar,
  }) : super(
          dob: dob,
          uid: uid,
          name: name,
          email: email,
          phone: phone,
          gender: gender,
          avatar: avatar,
          address: address,
          userType: userType,
          contacts: contacts,
          joinedDate: joinedDate,
          streamToken: streamToken,
          localAvatar: localAvatar,
          messageToken: messageToken,
        );

  @override
  Patient copyWith({
    String? bio,
    String? uid,
    String? name,
    DateTime? dob,
    String? phone,
    String? email,
    String? gender,
    String? avatar,
    Address? address,
    String? mobility,
    File? localAvatar,
    UserType? userType,
    DateTime? joinedDate,
    String? streamToken,
    String? messageToken,
    List<String>? agents,
    List<String>? contacts,
    String? maritalStatus,
    List<String>? allergies,
    List<String>? nextOfKin,
    Map<String, Meal>? meals,
    List<String>? conditions,
    DateTime? subscriptionEnds,
    List<String>? circleMembers,
    Map<String, String>? circle,
    WeightRecord? lastWeightRecord,
    Map<String, num>? hoursReceived,
    BloodGlucoseRecord? lastBloodGlucoseRecord,
    TemperatureRecord? lastTemperatureRecord,
    BloodPressureRecord? lastBloodPressureRecord,
    Map<String, AssignedCareProvider>? assignedCareProviders,
  }) {
    return Patient(
      dob: dob ?? this.dob!,
      uid: uid ?? this.uid,
      bio: bio ?? this.bio,
      name: name ?? this.name,
      email: email ?? this.email,
      meals: meals ?? this.meals,
      phone: phone ?? this.phone,
      avatar: avatar ?? this.avatar,
      circle: circle ?? this.circle,
      gender: gender ?? this.gender!,
      address: address ?? this.address!,
      userType: userType ?? this.userType,
      mobility: mobility ?? this.mobility,
      contacts: contacts ?? this.contacts,
      nextOfKin: nextOfKin ?? this.nextOfKin,
      allergies: allergies ?? this.allergies,
      conditions: conditions ?? this.conditions,
      joinedDate: joinedDate ?? this.joinedDate,
      streamToken: streamToken ?? this.streamToken,
      localAvatar: localAvatar ?? this.localAvatar,
      messageToken: messageToken ?? this.messageToken,
      maritalStatus: maritalStatus ?? this.maritalStatus,
      circleMembers: circleMembers ?? this.circleMembers,
      lastWeightRecord: lastWeightRecord ?? this.lastWeightRecord,
      lastBloodGlucoseRecord:
          lastBloodGlucoseRecord ?? this.lastBloodGlucoseRecord,
      lastTemperatureRecord:
          lastTemperatureRecord ?? this.lastTemperatureRecord,
      subscriptionEnds: subscriptionEnds ?? this.subscriptionEnds,
      lastBloodPressureRecord:
          lastBloodPressureRecord ?? this.lastBloodPressureRecord,
      assignedCareProviders:
          assignedCareProviders ?? this.assignedCareProviders,
    );
  }

  factory Patient.fromJson(Map<String, dynamic> json) =>
      _$PatientFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$PatientToJson(this);

  @override
  List<Object?> get props => <Object?>[
        uid,
        dob,
        bio,
        name,
        phone,
        avatar,
        email,
        meals,
        gender,
        circle,
        address,
        userType,
        contacts,
        allergies,
        mobility,
        nextOfKin,
        conditions,
        joinedDate,
        streamToken,
        localAvatar,
        messageToken,
        circleMembers,
        maritalStatus,
        lastWeightRecord,
        subscriptionEnds,
        lastBloodGlucoseRecord,
        lastTemperatureRecord,
        assignedCareProviders,
        lastBloodPressureRecord,
      ];
}
