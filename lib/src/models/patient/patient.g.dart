// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'patient.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Patient _$PatientFromJson(Map<String, dynamic> json) => Patient(
      email: json['email'] as String?,
      bio: json['bio'] as String,
      uid: json['uid'] as String,
      circle: Map<String, String>.from(json['circle'] as Map),
      name: json['name'] as String,
      gender: json['gender'] as String,
      dob: DateTime.parse(json['dob'] as String),
      phone: json['phone'] as String,
      mobility: json['mobility'] as String,
      avatar: json['avatar'] as String,
      lastWeightRecord: json['lastWeightRecord'] == null
          ? null
          : WeightRecord.fromJson(
              json['lastWeightRecord'] as Map<String, dynamic>),
      allergies:
          (json['allergies'] as List<dynamic>).map((e) => e as String).toList(),
      nextOfKin:
          (json['nextOfKin'] as List<dynamic>).map((e) => e as String).toList(),
      conditions: (json['conditions'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      address: Address.fromJson(json['address'] as Map<String, dynamic>),
      lastBloodGlucoseRecord: json['lastBloodGlucoseRecord'] == null
          ? null
          : BloodGlucoseRecord.fromJson(
              json['lastBloodGlucoseRecord'] as Map<String, dynamic>),
      lastTemperatureRecord: json['lastTemperatureRecord'] == null
          ? null
          : TemperatureRecord.fromJson(
              json['lastTemperatureRecord'] as Map<String, dynamic>),
      circleMembers: (json['circleMembers'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      streamToken: json['streamToken'] as String,
      lastBloodPressureRecord: json['lastBloodPressureRecord'] == null
          ? null
          : BloodPressureRecord.fromJson(
              json['lastBloodPressureRecord'] as Map<String, dynamic>),
      maritalStatus: json['maritalStatus'] as String,
      joinedDate: DateTime.parse(json['joinedDate'] as String),
      messageToken: json['messageToken'] as String,
      subscriptionEnds: DateTime.parse(json['subscriptionEnds'] as String),
      contacts:
          (json['contacts'] as List<dynamic>).map((e) => e as String).toList(),
      assignedCareProviders:
          (json['assignedCareProviders'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(
            k, AssignedCareProvider.fromJson(e as Map<String, dynamic>)),
      ),
      meals: (json['meals'] as Map<String, dynamic>?)?.map(
            (k, e) => MapEntry(k, Meal.fromJson(e as Map<String, dynamic>)),
          ) ??
          const <String, Meal>{},
      userType: $enumDecodeNullable(_$UserTypeEnumMap, json['userType']) ??
          UserType.patient,
    );

Map<String, dynamic> _$PatientToJson(Patient instance) => <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'phone': instance.phone,
      'avatar': instance.avatar,
      'gender': instance.gender,
      'email': instance.email,
      'dob': instance.dob?.toIso8601String(),
      'address': instance.address?.toJson(),
      'userType': _$UserTypeEnumMap[instance.userType]!,
      'streamToken': instance.streamToken,
      'joinedDate': instance.joinedDate.toIso8601String(),
      'messageToken': instance.messageToken,
      'contacts': instance.contacts,
      'bio': instance.bio,
      'mobility': instance.mobility,
      'maritalStatus': instance.maritalStatus,
      'nextOfKin': instance.nextOfKin,
      'allergies': instance.allergies,
      'conditions': instance.conditions,
      'meals': instance.meals.map((k, e) => MapEntry(k, e.toJson())),
      'subscriptionEnds': instance.subscriptionEnds.toIso8601String(),
      'circleMembers': instance.circleMembers,
      'circle': instance.circle,
      'lastWeightRecord': instance.lastWeightRecord?.toJson(),
      'lastBloodGlucoseRecord': instance.lastBloodGlucoseRecord?.toJson(),
      'lastTemperatureRecord': instance.lastTemperatureRecord?.toJson(),
      'lastBloodPressureRecord': instance.lastBloodPressureRecord?.toJson(),
      'assignedCareProviders':
          instance.assignedCareProviders.map((k, e) => MapEntry(k, e.toJson())),
    };

const _$UserTypeEnumMap = {
  UserType.user: 'user',
  UserType.patient: 'patient',
  UserType.careProvider: 'careProvider',
};
