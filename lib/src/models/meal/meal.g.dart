// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Meal _$MealFromJson(Map<String, dynamic> json) => Meal(
      meal: json['meal'] as String,
      type: json['type'] as String,
      time: DateTime.parse(json['time'] as String),
      patientId: json['patientId'] as String,
      careProviderId: json['careProviderId'] as String,
    );

Map<String, dynamic> _$MealToJson(Meal instance) => <String, dynamic>{
      'meal': instance.meal,
      'type': instance.type,
      'time': instance.time.toIso8601String(),
      'patientId': instance.patientId,
      'careProviderId': instance.careProviderId,
    };
