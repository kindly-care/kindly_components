import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'meal.g.dart';

@JsonSerializable()
class Meal extends Equatable {
  final String meal;
  final String type;
  final DateTime time;
  final String patientId;
  final String careProviderId;

  const Meal({
    required this.meal,
    required this.type,
    required this.time,
    required this.patientId,
    required this.careProviderId,
  });

  Meal copyWith({
    String? meal,
    String? type,
    DateTime? time,
    String? patientId,
    String? careProviderId,
  }) {
    return Meal(
      type: type ?? this.type,
      time: time ?? this.time,
      meal: meal ?? this.meal,
      patientId: patientId ?? this.patientId,
      careProviderId: careProviderId ?? this.careProviderId,
    );
  }

  factory Meal.fromJson(Map<String, dynamic> json) => _$MealFromJson(json);

  Map<String, dynamic> toJson() => _$MealToJson(this);

  @override
  List<Object?> get props => <Object?>[
        meal,
        time,
        type,
        patientId,
        careProviderId,
      ];

  @override
  bool get stringify => true;
}
