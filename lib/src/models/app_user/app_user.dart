import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../enums/enums.dart';
import '../address/address.dart';

part 'app_user.g.dart';

@JsonSerializable(explicitToJson: true)
class AppUser extends Equatable {
  final String uid;
  final String name;
  final String phone;
  final String avatar;
  final String? gender;
  final String? email;
  final DateTime? dob;
  final Address? address;
  @JsonKey(ignore: true)
  final File? localAvatar;
  final UserType userType;
  final String streamToken;
  final DateTime joinedDate;
  final String messageToken;
  final List<String> contacts;

  const AppUser({
    this.dob,
    this.email,
    this.gender,
    this.address,
    this.localAvatar,
    required this.uid,
    required this.name,
    required this.phone,
    required this.avatar,
    required this.contacts,
    required this.joinedDate,
    required this.streamToken,
    required this.messageToken,
    this.userType = UserType.user,
  });

  AppUser copyWith({
    String? uid,
    String? name,
    DateTime? dob,
    String? phone,
    String? email,
    String? gender,
    String? avatar,
    Address? address,
    File? localAvatar,
    UserType? userType,
    String? streamToken,
    String? messageToken,
    DateTime? joinedDate,
    List<String>? contacts,
  }) {
    return AppUser(
      dob: dob ?? this.dob,
      uid: uid ?? this.uid,
      name: name ?? this.name,
      phone: phone ?? this.phone,
      email: email ?? this.email,
      avatar: avatar ?? this.avatar,
      gender: gender ?? this.gender,
      address: address ?? this.address,
      contacts: contacts ?? this.contacts,
      userType: userType ?? this.userType,
      joinedDate: joinedDate ?? this.joinedDate,
      streamToken: streamToken ?? this.streamToken,
      localAvatar: localAvatar ?? this.localAvatar,
      messageToken: messageToken ?? this.messageToken,
    );
  }

  factory AppUser.fromJson(Map<String, dynamic> json) =>
      _$AppUserFromJson(json);

  Map<String, dynamic> toJson() => _$AppUserToJson(this);

  @override
  List<Object?> get props => <Object?>[
        uid,
        dob,
        name,
        phone,
        avatar,
        email,
        gender,
        address,
        userType,
        contacts,
        joinedDate,
        streamToken,
        localAvatar,
        messageToken
      ];
}
