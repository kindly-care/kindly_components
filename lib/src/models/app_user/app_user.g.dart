// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppUser _$AppUserFromJson(Map<String, dynamic> json) => AppUser(
      dob: json['dob'] == null ? null : DateTime.parse(json['dob'] as String),
      email: json['email'] as String?,
      gender: json['gender'] as String?,
      address: json['address'] == null
          ? null
          : Address.fromJson(json['address'] as Map<String, dynamic>),
      uid: json['uid'] as String,
      name: json['name'] as String,
      phone: json['phone'] as String,
      avatar: json['avatar'] as String,
      contacts:
          (json['contacts'] as List<dynamic>).map((e) => e as String).toList(),
      joinedDate: DateTime.parse(json['joinedDate'] as String),
      streamToken: json['streamToken'] as String,
      messageToken: json['messageToken'] as String,
      userType: $enumDecodeNullable(_$UserTypeEnumMap, json['userType']) ??
          UserType.user,
    );

Map<String, dynamic> _$AppUserToJson(AppUser instance) => <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'phone': instance.phone,
      'avatar': instance.avatar,
      'gender': instance.gender,
      'email': instance.email,
      'dob': instance.dob?.toIso8601String(),
      'address': instance.address?.toJson(),
      'userType': _$UserTypeEnumMap[instance.userType]!,
      'streamToken': instance.streamToken,
      'joinedDate': instance.joinedDate.toIso8601String(),
      'messageToken': instance.messageToken,
      'contacts': instance.contacts,
    };

const _$UserTypeEnumMap = {
  UserType.user: 'user',
  UserType.patient: 'patient',
  UserType.careProvider: 'careProvider',
};
