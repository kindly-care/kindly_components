// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'assigned_patient.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AssignedPatient _$AssignedPatientFromJson(Map<String, dynamic> json) =>
    AssignedPatient(
      shift: json['shift'] as String,
      patientId: json['patientId'] as String,
      patientName: json['patientName'] as String,
      assignedDate: DateTime.parse(json['assignedDate'] as String),
      careProviderId: json['careProviderId'] as String,
    );

Map<String, dynamic> _$AssignedPatientToJson(AssignedPatient instance) =>
    <String, dynamic>{
      'shift': instance.shift,
      'patientId': instance.patientId,
      'patientName': instance.patientName,
      'careProviderId': instance.careProviderId,
      'assignedDate': instance.assignedDate.toIso8601String(),
    };
