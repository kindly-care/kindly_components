import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'assigned_patient.g.dart';

@JsonSerializable()
class AssignedPatient extends Equatable {
  final String shift;
  final String patientId;
  final String patientName;
  final String careProviderId;
  final DateTime assignedDate;
  const AssignedPatient({
    required this.shift,
    required this.patientId,
    required this.patientName,
    required this.assignedDate,
    required this.careProviderId,
  });

  AssignedPatient copyWith({
    String? shift,
    String? patientId,
    String? patientName,
    DateTime? assignedDate,
    String? careProviderId,
  }) {
    return AssignedPatient(
      shift: shift ?? this.shift,
      patientId: patientId ?? this.patientId,
      patientName: patientName ?? this.patientName,
      assignedDate: assignedDate ?? this.assignedDate,
      careProviderId: careProviderId ?? this.careProviderId,
    );
  }

  factory AssignedPatient.fromJson(Map<String, dynamic> json) =>
      _$AssignedPatientFromJson(json);

  Map<String, dynamic> toJson() => _$AssignedPatientToJson(this);

  @override
  List<Object?> get props => <Object?>[
        shift,
        patientId,
        patientName,
        assignedDate,
        careProviderId,
      ];

  @override
  bool get stringify => true;
}
