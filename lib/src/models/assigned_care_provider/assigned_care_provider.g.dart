// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'assigned_care_provider.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AssignedCareProvider _$AssignedCareProviderFromJson(
        Map<String, dynamic> json) =>
    AssignedCareProvider(
      shift: json['shift'] as String,
      patientId: json['patientId'] as String,
      assignedDate: DateTime.parse(json['assignedDate'] as String),
      careProviderId: json['careProviderId'] as String,
      careProviderName: json['careProviderName'] as String,
      careProviderPhone: json['careProviderPhone'] as String,
    );

Map<String, dynamic> _$AssignedCareProviderToJson(
        AssignedCareProvider instance) =>
    <String, dynamic>{
      'shift': instance.shift,
      'patientId': instance.patientId,
      'careProviderId': instance.careProviderId,
      'assignedDate': instance.assignedDate.toIso8601String(),
      'careProviderName': instance.careProviderName,
      'careProviderPhone': instance.careProviderPhone,
    };
