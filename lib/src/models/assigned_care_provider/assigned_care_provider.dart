import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'assigned_care_provider.g.dart';

@JsonSerializable()
class AssignedCareProvider extends Equatable {
  final String shift;
  final String patientId;
  final String careProviderId;
  final DateTime assignedDate;
  final String careProviderName;
  final String careProviderPhone;

  const AssignedCareProvider({
    required this.shift,
    required this.patientId,
    required this.assignedDate,
    required this.careProviderId,
    required this.careProviderName,
    required this.careProviderPhone,
  });

  AssignedCareProvider copyWith({
    String? shift,
    int? numOfShifts,
    String? patientId,
    DateTime? assignedDate,
    String? careProviderId,
    String? careProviderName,
    String? careProviderPhone,
  }) {
    return AssignedCareProvider(
      shift: shift ?? this.shift,
      assignedDate: assignedDate ?? this.assignedDate,
      careProviderId: careProviderId ?? this.careProviderId,
      patientId: patientId ?? this.patientId,
      careProviderName: careProviderName ?? this.careProviderName,
      careProviderPhone: careProviderPhone ?? this.careProviderPhone,
    );
  }

  factory AssignedCareProvider.fromJson(Map<String, dynamic> json) =>
      _$AssignedCareProviderFromJson(json);

  Map<String, dynamic> toJson() => _$AssignedCareProviderToJson(this);

  @override
  List<Object?> get props => <Object?>[
        shift,
        patientId,
        assignedDate,
        careProviderId,
        careProviderName,
        careProviderPhone,
      ];

  @override
  bool get stringify => true;
}
