import 'package:json_annotation/json_annotation.dart';

import '../../../enums/enums.dart';
import '../push_notification.dart';

part 'interview_notification.g.dart';

@JsonSerializable()
class InterviewNotification extends PushNotification {
  final String shift;
  final bool isActedOn;
  final String position;
  final String patientId;
  final DateTime startDate;

  const InterviewNotification({
    required this.shift,
    this.isActedOn = false,
    required this.startDate,
    required this.patientId,
    required String id,
    bool isSeen = false,
    required String text,
    required String title,
    required this.position,
    required String authorId,
    required String authorName,
    required String authorPhone,
    required DateTime createdAt,
    required NotificationType type,
    required List<String> recipients,
  }) : super(
          id: id,
          type: type,
          text: text,
          title: title,
          isSeen: isSeen,
          authorId: authorId,
          createdAt: createdAt,
          authorName: authorName,
          recipients: recipients,
          authorPhone: authorPhone,
        );

  InterviewNotification copyWith({
    String? id,
    String? text,
    bool? isSeen,
    String? shift,
    String? title,
    bool? isActedOn,
    String? authorId,
    String? position,
    String? authorName,
    String? authorPhone,
    String? patientId,
    DateTime? createdAt,
    DateTime? startDate,
    NotificationType? type,
    List<String>? recipients,
  }) {
    return InterviewNotification(
      id: id ?? this.id,
      text: text ?? this.text,
      type: type ?? this.type,
      shift: shift ?? this.shift,
      title: title ?? this.title,
      isSeen: isSeen ?? this.isSeen,
      position: position ?? this.position,
      authorId: authorId ?? this.authorId,
      isActedOn: isActedOn ?? this.isActedOn,
      createdAt: createdAt ?? this.createdAt,
      patientId: patientId ?? this.patientId,
      startDate: startDate ?? this.startDate,
      authorName: authorName ?? this.authorName,
      authorPhone: authorPhone ?? this.authorPhone,
      recipients: recipients ?? this.recipients,
    );
  }

  factory InterviewNotification.fromJson(Map<String, dynamic> json) =>
      _$InterviewNotificationFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$InterviewNotificationToJson(this);

  @override
  List<Object?> get props => <Object?>[
        id,
        type,
        text,
        title,
        shift,
        isSeen,
        authorId,
        position,
        isActedOn,
        createdAt,
        patientId,
        startDate,
        authorName,
        authorPhone,
        recipients,
      ];

  @override
  bool get stringify => true;
}
