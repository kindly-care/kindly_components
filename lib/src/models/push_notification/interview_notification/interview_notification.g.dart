// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'interview_notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InterviewNotification _$InterviewNotificationFromJson(
        Map<String, dynamic> json) =>
    InterviewNotification(
      shift: json['shift'] as String,
      isActedOn: json['isActedOn'] as bool? ?? false,
      startDate: DateTime.parse(json['startDate'] as String),
      patientId: json['patientId'] as String,
      id: json['id'] as String,
      isSeen: json['isSeen'] as bool? ?? false,
      text: json['text'] as String,
      title: json['title'] as String,
      position: json['position'] as String,
      authorId: json['authorId'] as String,
      authorName: json['authorName'] as String,
      authorPhone: json['authorPhone'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      type: $enumDecode(_$NotificationTypeEnumMap, json['type']),
      recipients: (json['recipients'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$InterviewNotificationToJson(
        InterviewNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'isSeen': instance.isSeen,
      'title': instance.title,
      'authorId': instance.authorId,
      'authorName': instance.authorName,
      'authorPhone': instance.authorPhone,
      'createdAt': instance.createdAt.toIso8601String(),
      'recipients': instance.recipients,
      'type': _$NotificationTypeEnumMap[instance.type]!,
      'shift': instance.shift,
      'isActedOn': instance.isActedOn,
      'position': instance.position,
      'patientId': instance.patientId,
      'startDate': instance.startDate.toIso8601String(),
    };

const _$NotificationTypeEnumMap = {
  NotificationType.info: 'info',
  NotificationType.report: 'report',
  NotificationType.interviewRequest: 'interviewRequest',
  NotificationType.patientAssignment: 'patientAssignment',
};
