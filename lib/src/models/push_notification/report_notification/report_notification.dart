import 'package:json_annotation/json_annotation.dart';

import '../../../enums/enums.dart';
import '../push_notification.dart';

part 'report_notification.g.dart';

@JsonSerializable()
class ReportNotification extends PushNotification {
  final String reportId;
  final String patientId;
  const ReportNotification({
    bool isSeen = false,
    required String id,
    required String text,
    required String title,
    required this.reportId,
    required this.patientId,
    required String authorId,
    required String authorName,
    required String authorPhone,
    required DateTime createdAt,
    required List<String> recipients,
    NotificationType type = NotificationType.report,
  }) : super(
          id: id,
          type: type,
          text: text,
          title: title,
          isSeen: isSeen,
          authorId: authorId,
          createdAt: createdAt,
          authorName: authorName,
          recipients: recipients,
          authorPhone: authorPhone,
        );

  ReportNotification copyWith({
    String? id,
    String? text,
    bool? isSeen,
    String? title,
    String? reportId,
    String? authorId,
    String? patientId,
    String? authorName,
    String? authorPhone,
    DateTime? createdAt,
    NotificationType? type,
    List<String>? recipients,
  }) {
    return ReportNotification(
      id: id ?? this.id,
      text: text ?? this.text,
      type: type ?? this.type,
      title: title ?? this.title,
      isSeen: isSeen ?? this.isSeen,
      authorId: authorId ?? this.authorId,
      reportId: reportId ?? this.reportId,
      patientId: patientId ?? this.patientId,
      createdAt: createdAt ?? this.createdAt,
      recipients: recipients ?? this.recipients,
      authorName: authorName ?? this.authorName,
      authorPhone: authorPhone ?? this.authorPhone,
    );
  }

  factory ReportNotification.fromJson(Map<String, dynamic> json) =>
      _$ReportNotificationFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ReportNotificationToJson(this);

  @override
  List<Object?> get props => <Object?>[
        id,
        type,
        text,
        title,
        isSeen,
        authorId,
        reportId,
        patientId,
        createdAt,
        authorName,
        recipients,
        authorPhone,
      ];

  @override
  bool get stringify => true;
}
