// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReportNotification _$ReportNotificationFromJson(Map<String, dynamic> json) =>
    ReportNotification(
      isSeen: json['isSeen'] as bool? ?? false,
      id: json['id'] as String,
      text: json['text'] as String,
      title: json['title'] as String,
      reportId: json['reportId'] as String,
      patientId: json['patientId'] as String,
      authorId: json['authorId'] as String,
      authorName: json['authorName'] as String,
      authorPhone: json['authorPhone'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      recipients: (json['recipients'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      type: $enumDecodeNullable(_$NotificationTypeEnumMap, json['type']) ??
          NotificationType.report,
    );

Map<String, dynamic> _$ReportNotificationToJson(ReportNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'isSeen': instance.isSeen,
      'title': instance.title,
      'authorId': instance.authorId,
      'authorName': instance.authorName,
      'authorPhone': instance.authorPhone,
      'createdAt': instance.createdAt.toIso8601String(),
      'recipients': instance.recipients,
      'type': _$NotificationTypeEnumMap[instance.type]!,
      'reportId': instance.reportId,
      'patientId': instance.patientId,
    };

const _$NotificationTypeEnumMap = {
  NotificationType.info: 'info',
  NotificationType.report: 'report',
  NotificationType.interviewRequest: 'interviewRequest',
  NotificationType.patientAssignment: 'patientAssignment',
};
