// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'push_notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PushNotification _$PushNotificationFromJson(Map<String, dynamic> json) =>
    PushNotification(
      id: json['id'] as String,
      text: json['text'] as String,
      title: json['title'] as String,
      isSeen: json['isSeen'] as bool? ?? false,
      authorId: json['authorId'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      authorName: json['authorName'] as String,
      authorPhone: json['authorPhone'] as String,
      recipients: (json['recipients'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      type: $enumDecodeNullable(_$NotificationTypeEnumMap, json['type']) ??
          NotificationType.info,
    );

Map<String, dynamic> _$PushNotificationToJson(PushNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'isSeen': instance.isSeen,
      'title': instance.title,
      'authorId': instance.authorId,
      'authorName': instance.authorName,
      'authorPhone': instance.authorPhone,
      'createdAt': instance.createdAt.toIso8601String(),
      'recipients': instance.recipients,
      'type': _$NotificationTypeEnumMap[instance.type]!,
    };

const _$NotificationTypeEnumMap = {
  NotificationType.info: 'info',
  NotificationType.report: 'report',
  NotificationType.interviewRequest: 'interviewRequest',
  NotificationType.patientAssignment: 'patientAssignment',
};
