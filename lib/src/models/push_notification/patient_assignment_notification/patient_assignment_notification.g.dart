// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'patient_assignment_notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PatientAssignmentNotification _$PatientAssignmentNotificationFromJson(
        Map<String, dynamic> json) =>
    PatientAssignmentNotification(
      numOfShifts: json['numOfShifts'] as int?,
      shift: json['shift'] as String,
      isActedOn: json['isActedOn'] as bool? ?? false,
      startDate: DateTime.parse(json['startDate'] as String),
      patientId: json['patientId'] as String,
      id: json['id'] as String,
      isSeen: json['isSeen'] as bool? ?? false,
      text: json['text'] as String,
      title: json['title'] as String,
      authorId: json['authorId'] as String,
      authorName: json['authorName'] as String,
      authorPhone: json['authorPhone'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      type: $enumDecode(_$NotificationTypeEnumMap, json['type']),
      recipients: (json['recipients'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$PatientAssignmentNotificationToJson(
        PatientAssignmentNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'isSeen': instance.isSeen,
      'title': instance.title,
      'authorId': instance.authorId,
      'authorName': instance.authorName,
      'authorPhone': instance.authorPhone,
      'createdAt': instance.createdAt.toIso8601String(),
      'recipients': instance.recipients,
      'type': _$NotificationTypeEnumMap[instance.type]!,
      'shift': instance.shift,
      'isActedOn': instance.isActedOn,
      'patientId': instance.patientId,
      'numOfShifts': instance.numOfShifts,
      'startDate': instance.startDate.toIso8601String(),
    };

const _$NotificationTypeEnumMap = {
  NotificationType.info: 'info',
  NotificationType.report: 'report',
  NotificationType.interviewRequest: 'interviewRequest',
  NotificationType.patientAssignment: 'patientAssignment',
};
