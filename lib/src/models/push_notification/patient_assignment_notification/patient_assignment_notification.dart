import 'package:json_annotation/json_annotation.dart';

import '../../../enums/enums.dart';
import '../push_notification.dart';

part 'patient_assignment_notification.g.dart';

@JsonSerializable()
class PatientAssignmentNotification extends PushNotification {
  final String shift;
  final bool isActedOn;
  final String patientId;
  final int? numOfShifts;
  final DateTime startDate;

  const PatientAssignmentNotification({
    this.numOfShifts,
    required this.shift,
    this.isActedOn = false,
    required this.startDate,
    required this.patientId,
    required String id,
    bool isSeen = false,
    required String text,
    required String title,
    required String authorId,
    required String authorName,
    required String authorPhone,
    required DateTime createdAt,
    required NotificationType type,
    required List<String> recipients,
  }) : super(
          id: id,
          text: text,
          type: type,
          title: title,
          isSeen: isSeen,
          authorId: authorId,
          createdAt: createdAt,
          authorName: authorName,
          recipients: recipients,
          authorPhone: authorPhone,
        );

  PatientAssignmentNotification copyWith({
    String? id,
    String? text,
    bool? isSeen,
    String? shift,
    String? title,
    bool? isActedOn,
    String? authorId,
    String? patientId,
    int? numOfShifts,
    String? authorName,
    String? authorPhone,
    DateTime? createdAt,
    DateTime? startDate,
    NotificationType? type,
    List<String>? recipients,
  }) {
    return PatientAssignmentNotification(
      id: id ?? this.id,
      type: type ?? this.type,
      text: text ?? this.text,
      shift: shift ?? this.shift,
      title: title ?? this.title,
      isSeen: isSeen ?? this.isSeen,
      authorId: authorId ?? this.authorId,
      isActedOn: isActedOn ?? this.isActedOn,
      createdAt: createdAt ?? this.createdAt,
      patientId: patientId ?? this.patientId,
      startDate: startDate ?? this.startDate,
      authorName: authorName ?? this.authorName,
      authorPhone: authorPhone ?? this.authorPhone,
      recipients: recipients ?? this.recipients,
      numOfShifts: numOfShifts ?? this.numOfShifts,
    );
  }

  factory PatientAssignmentNotification.fromJson(Map<String, dynamic> json) =>
      _$PatientAssignmentNotificationFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$PatientAssignmentNotificationToJson(this);

  @override
  List<Object?> get props => <Object?>[
        id,
        type,
        text,
        title,
        shift,
        isSeen,
        authorId,
        isActedOn,
        createdAt,
        patientId,
        startDate,
        authorName,
        authorPhone,
        recipients,
        numOfShifts,
      ];

  @override
  bool get stringify => true;
}
