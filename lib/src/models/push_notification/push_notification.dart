import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../enums/enums.dart';
import 'interview_notification/interview_notification.dart';
import 'patient_assignment_notification/patient_assignment_notification.dart';
import 'report_notification/report_notification.dart';

part 'push_notification.g.dart';

@JsonSerializable()
class PushNotification extends Equatable {
  final String id;
  final String text;
  final bool isSeen;
  final String title;
  final String authorId;
  final String authorName;
  final String authorPhone;
  final DateTime createdAt;
  final List<String> recipients;
  final NotificationType type;
  const PushNotification({
    required this.id,
    required this.text,
    required this.title,
    this.isSeen = false,
    required this.authorId,
    required this.createdAt,
    required this.authorName,
    required this.authorPhone,
    required this.recipients,
    this.type = NotificationType.info,
  });

  factory PushNotification.fromJson(Map<String, dynamic> json) =>
      _$PushNotificationFromJson(json);

  factory PushNotification.fromMap(Map<String, dynamic> json) {
    final String type = json['type'] as String;
    if (type == 'interviewRequest') {
      return InterviewNotification.fromJson(json);
    } else if (type == 'patientAssignment') {
      return PatientAssignmentNotification.fromJson(json);
    } else if (type == 'report') {
      return ReportNotification.fromJson(json);
    } else {
      return PushNotification.fromJson(json);
    }
  }

  Map<String, dynamic> toMap() {
    final PushNotification notification = this;
    if (notification is InterviewNotification) {
      return notification.toJson();
    } else if (notification is PatientAssignmentNotification) {
      return notification.toJson();
    } else if (notification is ReportNotification) {
      return notification.toJson();
    } else {
      return notification.toJson();
    }
  }

  Map<String, dynamic> toJson() => _$PushNotificationToJson(this);

  @override
  List<Object?> get props => <Object?>[
        id,
        type,
        text,
        title,
        isSeen,
        authorId,
        createdAt,
        authorName,
        authorPhone,
        recipients,
      ];
}
