import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'time_log.g.dart';

@HiveType(typeId: 1)
@JsonSerializable()
class TimeLog extends Equatable {
  @HiveField(0)
  final String careProviderId;
  @HiveField(1)
  final String careProviderName;
  @HiveField(2)
  final String patientId;
  @HiveField(3)
  final String careProviderPhone;
  @HiveField(4)
  final String dateCreated;
  @HiveField(5)
  final String patientName;
  @HiveField(6)
  final DateTime? startTime;
  @HiveField(7)
  final DateTime? finishTime;
  @HiveField(8)
  final String timeLogId;

  const TimeLog({
    this.startTime,
    this.finishTime,
    required this.careProviderId,
    required this.careProviderName,
    required this.patientId,
    required this.careProviderPhone,
    required this.dateCreated,
    required this.patientName,
    required this.timeLogId,
  });

  TimeLog copyWith({
    bool? paid,
    String? careProviderId,
    String? careProviderName,
    String? patientId,
    String? careProviderPhone,
    String? dateCreated,
    String? patientName,
    DateTime? startTime,
    DateTime? finishTime,
    String? timeLogId,
  }) {
    return TimeLog(
      careProviderId: careProviderId ?? this.careProviderId,
      careProviderName: careProviderName ?? this.careProviderName,
      startTime: startTime ?? this.startTime,
      patientId: patientId ?? this.patientId,
      finishTime: finishTime ?? this.finishTime,
      careProviderPhone: careProviderPhone ?? this.careProviderPhone,
      patientName: patientName ?? this.patientName,
      dateCreated: dateCreated ?? this.dateCreated,
      timeLogId: timeLogId ?? this.timeLogId,
    );
  }

  factory TimeLog.fromJson(Map<String, dynamic> json) =>
      _$TimeLogFromJson(json);

  Map<String, dynamic> toJson() => _$TimeLogToJson(this);

  @override
  List<Object?> get props {
    return <Object?>[
      careProviderId,
      careProviderName,
      startTime,
      patientId,
      finishTime,
      careProviderPhone,
      dateCreated,
      patientName,
      timeLogId,
    ];
  }

  @override
  bool get stringify => true;
}
