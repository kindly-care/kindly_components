// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'time_log.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TimeLogAdapter extends TypeAdapter<TimeLog> {
  @override
  final int typeId = 1;

  @override
  TimeLog read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TimeLog(
      startTime: fields[6] as DateTime?,
      finishTime: fields[7] as DateTime?,
      careProviderId: fields[0] as String,
      careProviderName: fields[1] as String,
      patientId: fields[2] as String,
      careProviderPhone: fields[3] as String,
      dateCreated: fields[4] as String,
      patientName: fields[5] as String,
      timeLogId: fields[8] as String,
    );
  }

  @override
  void write(BinaryWriter writer, TimeLog obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.careProviderId)
      ..writeByte(1)
      ..write(obj.careProviderName)
      ..writeByte(2)
      ..write(obj.patientId)
      ..writeByte(3)
      ..write(obj.careProviderPhone)
      ..writeByte(4)
      ..write(obj.dateCreated)
      ..writeByte(5)
      ..write(obj.patientName)
      ..writeByte(6)
      ..write(obj.startTime)
      ..writeByte(7)
      ..write(obj.finishTime)
      ..writeByte(8)
      ..write(obj.timeLogId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimeLogAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TimeLog _$TimeLogFromJson(Map<String, dynamic> json) => TimeLog(
      startTime: json['startTime'] == null
          ? null
          : DateTime.parse(json['startTime'] as String),
      finishTime: json['finishTime'] == null
          ? null
          : DateTime.parse(json['finishTime'] as String),
      careProviderId: json['careProviderId'] as String,
      careProviderName: json['careProviderName'] as String,
      patientId: json['patientId'] as String,
      careProviderPhone: json['careProviderPhone'] as String,
      dateCreated: json['dateCreated'] as String,
      patientName: json['patientName'] as String,
      timeLogId: json['timeLogId'] as String,
    );

Map<String, dynamic> _$TimeLogToJson(TimeLog instance) => <String, dynamic>{
      'careProviderId': instance.careProviderId,
      'careProviderName': instance.careProviderName,
      'patientId': instance.patientId,
      'careProviderPhone': instance.careProviderPhone,
      'dateCreated': instance.dateCreated,
      'patientName': instance.patientName,
      'startTime': instance.startTime?.toIso8601String(),
      'finishTime': instance.finishTime?.toIso8601String(),
      'timeLogId': instance.timeLogId,
    };
