import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'blood_pressure_record.g.dart';

@HiveType(typeId: 11)
@JsonSerializable()
class BloodPressureRecord extends Equatable {
  @HiveField(0)
  final num systolic;
  @HiveField(1)
  final num diastolic;
  @HiveField(2)
  final String patientId;
  @HiveField(3)
  final DateTime takenAt;
  @HiveField(4)
  final String dateCreated;
  @HiveField(5)
  final String careProviderId;
  @HiveField(6)
  final String careProviderName;

  const BloodPressureRecord({
    required this.takenAt,
    required this.systolic,
    required this.diastolic,
    required this.patientId,
    required this.dateCreated,
    required this.careProviderId,
    required this.careProviderName,
  });

  BloodPressureRecord copyWith({
    num? systolic,
    num? diastolic,
    DateTime? takenAt,
    String? patientId,
    String? dateCreated,
    String? careProviderId,
    String? careProviderName,
  }) {
    return BloodPressureRecord(
      takenAt: takenAt ?? this.takenAt,
      systolic: systolic ?? this.systolic,
      diastolic: diastolic ?? this.diastolic,
      patientId: patientId ?? this.patientId,
      dateCreated: dateCreated ?? this.dateCreated,
      careProviderId: careProviderId ?? this.careProviderId,
      careProviderName: careProviderName ?? this.careProviderName,
    );
  }

  factory BloodPressureRecord.fromJson(Map<String, dynamic> json) =>
      _$BloodPressureRecordFromJson(json);

  Map<String, dynamic> toJson() => _$BloodPressureRecordToJson(this);

  @override
  List<Object?> get props => <Object?>[
        systolic,
        takenAt,
        diastolic,
        patientId,
        dateCreated,
        careProviderId,
        careProviderName,
      ];

  @override
  bool get stringify => true;
}
