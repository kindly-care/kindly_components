// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'blood_pressure_record.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BloodPressureRecordAdapter extends TypeAdapter<BloodPressureRecord> {
  @override
  final int typeId = 11;

  @override
  BloodPressureRecord read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BloodPressureRecord(
      takenAt: fields[3] as DateTime,
      systolic: fields[0] as num,
      diastolic: fields[1] as num,
      patientId: fields[2] as String,
      dateCreated: fields[4] as String,
      careProviderId: fields[5] as String,
      careProviderName: fields[6] as String,
    );
  }

  @override
  void write(BinaryWriter writer, BloodPressureRecord obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.systolic)
      ..writeByte(1)
      ..write(obj.diastolic)
      ..writeByte(2)
      ..write(obj.patientId)
      ..writeByte(3)
      ..write(obj.takenAt)
      ..writeByte(4)
      ..write(obj.dateCreated)
      ..writeByte(5)
      ..write(obj.careProviderId)
      ..writeByte(6)
      ..write(obj.careProviderName);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BloodPressureRecordAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BloodPressureRecord _$BloodPressureRecordFromJson(Map<String, dynamic> json) =>
    BloodPressureRecord(
      takenAt: DateTime.parse(json['takenAt'] as String),
      systolic: json['systolic'] as num,
      diastolic: json['diastolic'] as num,
      patientId: json['patientId'] as String,
      dateCreated: json['dateCreated'] as String,
      careProviderId: json['careProviderId'] as String,
      careProviderName: json['careProviderName'] as String,
    );

Map<String, dynamic> _$BloodPressureRecordToJson(
        BloodPressureRecord instance) =>
    <String, dynamic>{
      'systolic': instance.systolic,
      'diastolic': instance.diastolic,
      'patientId': instance.patientId,
      'takenAt': instance.takenAt.toIso8601String(),
      'dateCreated': instance.dateCreated,
      'careProviderId': instance.careProviderId,
      'careProviderName': instance.careProviderName,
    };
