import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weight_record.g.dart';

@HiveType(typeId: 5)
@JsonSerializable()
class WeightRecord extends Equatable {
  @HiveField(0)
  final num weight;
  @HiveField(1)
  final String patientId;
  @HiveField(2)
  final DateTime takenAt;
  @HiveField(3)
  final String dateCreated;
  @HiveField(4)
  final String careProviderId;
  @HiveField(5)
  final String careProviderName;

  const WeightRecord({
    required this.weight,
    required this.takenAt,
    required this.patientId,
    required this.dateCreated,
    required this.careProviderId,
    required this.careProviderName,
  });

  WeightRecord copyWith({
    num? weight,
    DateTime? takenAt,
    String? patientId,
    String? dateCreated,
    String? careProviderId,
    String? careProviderName,
  }) {
    return WeightRecord(
      weight: weight ?? this.weight,
      takenAt: takenAt ?? this.takenAt,
      patientId: patientId ?? this.patientId,
      dateCreated: dateCreated ?? this.dateCreated,
      careProviderId: careProviderId ?? this.careProviderId,
      careProviderName: careProviderName ?? this.careProviderName,
    );
  }

  factory WeightRecord.fromJson(Map<String, dynamic> json) =>
      _$WeightRecordFromJson(json);

  Map<String, dynamic> toJson() => _$WeightRecordToJson(this);

  @override
  List<Object?> get props => <Object?>[
        weight,
        takenAt,
        patientId,
        dateCreated,
        careProviderId,
        careProviderName,
      ];

  @override
  bool get stringify => true;
}
