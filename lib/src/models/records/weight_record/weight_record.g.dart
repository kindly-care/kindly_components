// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weight_record.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class WeightRecordAdapter extends TypeAdapter<WeightRecord> {
  @override
  final int typeId = 5;

  @override
  WeightRecord read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return WeightRecord(
      weight: fields[0] as num,
      takenAt: fields[2] as DateTime,
      patientId: fields[1] as String,
      dateCreated: fields[3] as String,
      careProviderId: fields[4] as String,
      careProviderName: fields[5] as String,
    );
  }

  @override
  void write(BinaryWriter writer, WeightRecord obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.weight)
      ..writeByte(1)
      ..write(obj.patientId)
      ..writeByte(2)
      ..write(obj.takenAt)
      ..writeByte(3)
      ..write(obj.dateCreated)
      ..writeByte(4)
      ..write(obj.careProviderId)
      ..writeByte(5)
      ..write(obj.careProviderName);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WeightRecordAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeightRecord _$WeightRecordFromJson(Map<String, dynamic> json) => WeightRecord(
      weight: json['weight'] as num,
      takenAt: DateTime.parse(json['takenAt'] as String),
      patientId: json['patientId'] as String,
      dateCreated: json['dateCreated'] as String,
      careProviderId: json['careProviderId'] as String,
      careProviderName: json['careProviderName'] as String,
    );

Map<String, dynamic> _$WeightRecordToJson(WeightRecord instance) =>
    <String, dynamic>{
      'weight': instance.weight,
      'patientId': instance.patientId,
      'takenAt': instance.takenAt.toIso8601String(),
      'dateCreated': instance.dateCreated,
      'careProviderId': instance.careProviderId,
      'careProviderName': instance.careProviderName,
    };
