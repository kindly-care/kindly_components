// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'blood_glucose_record.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BloodGlucoseRecordAdapter extends TypeAdapter<BloodGlucoseRecord> {
  @override
  final int typeId = 10;

  @override
  BloodGlucoseRecord read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BloodGlucoseRecord(
      takenAt: fields[2] as DateTime,
      patientId: fields[1] as String,
      bloodGlucose: fields[0] as num,
      dateCreated: fields[3] as String,
      careProviderId: fields[4] as String,
      careProviderName: fields[5] as String,
    );
  }

  @override
  void write(BinaryWriter writer, BloodGlucoseRecord obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.bloodGlucose)
      ..writeByte(1)
      ..write(obj.patientId)
      ..writeByte(2)
      ..write(obj.takenAt)
      ..writeByte(3)
      ..write(obj.dateCreated)
      ..writeByte(4)
      ..write(obj.careProviderId)
      ..writeByte(5)
      ..write(obj.careProviderName);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BloodGlucoseRecordAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BloodGlucoseRecord _$BloodGlucoseRecordFromJson(Map<String, dynamic> json) =>
    BloodGlucoseRecord(
      takenAt: DateTime.parse(json['takenAt'] as String),
      patientId: json['patientId'] as String,
      bloodGlucose: json['bloodGlucose'] as num,
      dateCreated: json['dateCreated'] as String,
      careProviderId: json['careProviderId'] as String,
      careProviderName: json['careProviderName'] as String,
    );

Map<String, dynamic> _$BloodGlucoseRecordToJson(BloodGlucoseRecord instance) =>
    <String, dynamic>{
      'bloodGlucose': instance.bloodGlucose,
      'patientId': instance.patientId,
      'takenAt': instance.takenAt.toIso8601String(),
      'dateCreated': instance.dateCreated,
      'careProviderId': instance.careProviderId,
      'careProviderName': instance.careProviderName,
    };
