import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'blood_glucose_record.g.dart';

@HiveType(typeId: 10)
@JsonSerializable()
class BloodGlucoseRecord extends Equatable {
  @HiveField(0)
  final num bloodGlucose;
  @HiveField(1)
  final String patientId;
  @HiveField(2)
  final DateTime takenAt;
  @HiveField(3)
  final String dateCreated;
  @HiveField(4)
  final String careProviderId;
  @HiveField(5)
  final String careProviderName;

  const BloodGlucoseRecord({
    required this.takenAt,
    required this.patientId,
    required this.bloodGlucose,
    required this.dateCreated,
    required this.careProviderId,
    required this.careProviderName,
  });

  BloodGlucoseRecord copyWith({
    num? bloodGlucose,
    DateTime? takenAt,
    String? patientId,
    String? dateCreated,
    String? careProviderId,
    String? careProviderName,
  }) {
    return BloodGlucoseRecord(
      takenAt: takenAt ?? this.takenAt,
      patientId: patientId ?? this.patientId,
      dateCreated: dateCreated ?? this.dateCreated,
      bloodGlucose: bloodGlucose ?? this.bloodGlucose,
      careProviderId: careProviderId ?? this.careProviderId,
      careProviderName: careProviderName ?? this.careProviderName,
    );
  }

  factory BloodGlucoseRecord.fromJson(Map<String, dynamic> json) =>
      _$BloodGlucoseRecordFromJson(json);

  Map<String, dynamic> toJson() => _$BloodGlucoseRecordToJson(this);

  @override
  List<Object?> get props => <Object?>[
        takenAt,
        patientId,
        dateCreated,
        bloodGlucose,
        careProviderId,
        careProviderName,
      ];

  @override
  bool get stringify => true;
}
