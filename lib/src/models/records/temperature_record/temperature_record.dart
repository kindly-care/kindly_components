import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'temperature_record.g.dart';

@HiveType(typeId: 9)
@JsonSerializable()
class TemperatureRecord extends Equatable {
  @HiveField(0)
  final num temperature;
  @HiveField(1)
  final String patientId;
  @HiveField(2)
  final DateTime takenAt;
  @HiveField(3)
  final String dateCreated;
  @HiveField(4)
  final String careProviderId;
  @HiveField(5)
  final String careProviderName;

  const TemperatureRecord({
    required this.takenAt,
    required this.patientId,
    required this.temperature,
    required this.dateCreated,
    required this.careProviderId,
    required this.careProviderName,
  });

  TemperatureRecord copyWith({
    String? careProviderId,
    num? temperature,
    String? careProviderName,
    DateTime? takenAt,
    String? patientId,
    String? dateCreated,
  }) {
    return TemperatureRecord(
      takenAt: takenAt ?? this.takenAt,
      patientId: patientId ?? this.patientId,
      temperature: temperature ?? this.temperature,
      dateCreated: dateCreated ?? this.dateCreated,
      careProviderId: careProviderId ?? this.careProviderId,
      careProviderName: careProviderName ?? this.careProviderName,
    );
  }

  factory TemperatureRecord.fromJson(Map<String, dynamic> json) =>
      _$TemperatureRecordFromJson(json);

  Map<String, dynamic> toJson() => _$TemperatureRecordToJson(this);

  @override
  List<Object?> get props => <Object?>[
        takenAt,
        patientId,
        temperature,
        dateCreated,
        careProviderId,
        careProviderName,
      ];

  @override
  bool get stringify => true;
}
