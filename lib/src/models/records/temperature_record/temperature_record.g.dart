// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'temperature_record.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TemperatureRecordAdapter extends TypeAdapter<TemperatureRecord> {
  @override
  final int typeId = 9;

  @override
  TemperatureRecord read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TemperatureRecord(
      takenAt: fields[2] as DateTime,
      patientId: fields[1] as String,
      temperature: fields[0] as num,
      dateCreated: fields[3] as String,
      careProviderId: fields[4] as String,
      careProviderName: fields[5] as String,
    );
  }

  @override
  void write(BinaryWriter writer, TemperatureRecord obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.temperature)
      ..writeByte(1)
      ..write(obj.patientId)
      ..writeByte(2)
      ..write(obj.takenAt)
      ..writeByte(3)
      ..write(obj.dateCreated)
      ..writeByte(4)
      ..write(obj.careProviderId)
      ..writeByte(5)
      ..write(obj.careProviderName);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TemperatureRecordAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TemperatureRecord _$TemperatureRecordFromJson(Map<String, dynamic> json) =>
    TemperatureRecord(
      takenAt: DateTime.parse(json['takenAt'] as String),
      patientId: json['patientId'] as String,
      temperature: json['temperature'] as num,
      dateCreated: json['dateCreated'] as String,
      careProviderId: json['careProviderId'] as String,
      careProviderName: json['careProviderName'] as String,
    );

Map<String, dynamic> _$TemperatureRecordToJson(TemperatureRecord instance) =>
    <String, dynamic>{
      'temperature': instance.temperature,
      'patientId': instance.patientId,
      'takenAt': instance.takenAt.toIso8601String(),
      'dateCreated': instance.dateCreated,
      'careProviderId': instance.careProviderId,
      'careProviderName': instance.careProviderName,
    };
