import 'package:json_annotation/json_annotation.dart';

import '../../../enums/enums.dart';
import '../care_task/care_task.dart';

part 'meal_task.g.dart';

@JsonSerializable()
class MealTask extends CareTask {
  final String mealType;
  final bool? requestImages;

  const MealTask({
    String? description,
    required String title,
    required DateTime time,
    required this.mealType,
    required String taskId,
    required String patientId,
    this.requestImages = false,
    required DateTime lastUpdated,
    required String lastUpdatedBy,
    required List<String> repeatDays,
    CareTaskType type = CareTaskType.meal,
  }) : super(
          type: type,
          time: time,
          title: title,
          taskId: taskId,
          patientId: patientId,
          repeatDays: repeatDays,
          description: description,
          lastUpdated: lastUpdated,
          lastUpdatedBy: lastUpdatedBy,
        );

  @override
  MealTask copyWith({
    String? taskId,
    String? title,
    DateTime? time,
    String? mealType,
    String? patientId,
    CareTaskType? type,
    String? description,
    bool? requestImages,
    DateTime? lastUpdated,
    String? lastUpdatedBy,
    List<String>? repeatDays,
  }) {
    return MealTask(
      time: time ?? this.time,
      type: type ?? this.type,
      title: title ?? this.title,
      taskId: taskId ?? this.taskId,
      mealType: mealType ?? this.mealType,
      patientId: patientId ?? this.patientId,
      repeatDays: repeatDays ?? this.repeatDays,
      description: description ?? this.description,
      lastUpdated: lastUpdated ?? this.lastUpdated,
      lastUpdatedBy: lastUpdatedBy ?? this.lastUpdatedBy,
      requestImages: requestImages ?? this.requestImages,
    );
  }

  factory MealTask.fromJson(Map<String, dynamic> json) =>
      _$MealTaskFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$MealTaskToJson(this);

  @override
  List<Object?> get props => <Object?>[
        time,
        type,
        title,
        taskId,
        mealType,
        patientId,
        repeatDays,
        lastUpdated,
        description,
        lastUpdatedBy,
        requestImages,
      ];

  @override
  bool get stringify => true;
}
