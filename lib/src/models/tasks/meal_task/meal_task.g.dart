// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meal_task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MealTask _$MealTaskFromJson(Map<String, dynamic> json) => MealTask(
      description: json['description'] as String?,
      title: json['title'] as String,
      time: DateTime.parse(json['time'] as String),
      mealType: json['mealType'] as String,
      taskId: json['taskId'] as String,
      patientId: json['patientId'] as String,
      requestImages: json['requestImages'] as bool? ?? false,
      lastUpdated: DateTime.parse(json['lastUpdated'] as String),
      lastUpdatedBy: json['lastUpdatedBy'] as String,
      repeatDays: (json['repeatDays'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      type: $enumDecodeNullable(_$CareTaskTypeEnumMap, json['type']) ??
          CareTaskType.meal,
    );

Map<String, dynamic> _$MealTaskToJson(MealTask instance) => <String, dynamic>{
      'title': instance.title,
      'taskId': instance.taskId,
      'time': instance.time.toIso8601String(),
      'patientId': instance.patientId,
      'type': _$CareTaskTypeEnumMap[instance.type]!,
      'description': instance.description,
      'lastUpdated': instance.lastUpdated.toIso8601String(),
      'lastUpdatedBy': instance.lastUpdatedBy,
      'repeatDays': instance.repeatDays,
      'mealType': instance.mealType,
      'requestImages': instance.requestImages,
    };

const _$CareTaskTypeEnumMap = {
  CareTaskType.general: 'general',
  CareTaskType.meal: 'meal',
  CareTaskType.medication: 'medication',
};
