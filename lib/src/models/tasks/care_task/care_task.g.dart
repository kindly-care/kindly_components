// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'care_task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CareTask _$CareTaskFromJson(Map<String, dynamic> json) => CareTask(
      description: json['description'] as String?,
      time: DateTime.parse(json['time'] as String),
      type: $enumDecode(_$CareTaskTypeEnumMap, json['type']),
      title: json['title'] as String,
      taskId: json['taskId'] as String,
      patientId: json['patientId'] as String,
      repeatDays: (json['repeatDays'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      lastUpdated: DateTime.parse(json['lastUpdated'] as String),
      lastUpdatedBy: json['lastUpdatedBy'] as String,
    );

Map<String, dynamic> _$CareTaskToJson(CareTask instance) => <String, dynamic>{
      'title': instance.title,
      'taskId': instance.taskId,
      'time': instance.time.toIso8601String(),
      'patientId': instance.patientId,
      'type': _$CareTaskTypeEnumMap[instance.type]!,
      'description': instance.description,
      'lastUpdated': instance.lastUpdated.toIso8601String(),
      'lastUpdatedBy': instance.lastUpdatedBy,
      'repeatDays': instance.repeatDays,
    };

const _$CareTaskTypeEnumMap = {
  CareTaskType.general: 'general',
  CareTaskType.meal: 'meal',
  CareTaskType.medication: 'medication',
};
