import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../enums/enums.dart';

part 'care_task.g.dart';

@JsonSerializable(explicitToJson: true)
class CareTask extends Equatable {
  final String title;
  final String taskId;
  final DateTime time;
  final String patientId;
  final CareTaskType type;
  final String? description;
  final DateTime lastUpdated;
  final String lastUpdatedBy;
  final List<String> repeatDays;
  const CareTask({
    this.description,
    required this.time,
    required this.type,
    required this.title,
    required this.taskId,
    required this.patientId,
    required this.repeatDays,
    required this.lastUpdated,
    required this.lastUpdatedBy,
  });

  CareTask copyWith({
    String? taskId,
    String? title,
    DateTime? time,
    String? patientId,
    CareTaskType? type,
    String? description,
    DateTime? lastUpdated,
    String? lastUpdatedBy,
    List<String>? repeatDays,
  }) {
    return CareTask(
      time: time ?? this.time,
      type: type ?? this.type,
      title: title ?? this.title,
      taskId: taskId ?? this.taskId,
      patientId: patientId ?? this.patientId,
      repeatDays: repeatDays ?? this.repeatDays,
      description: description ?? this.description,
      lastUpdated: lastUpdated ?? this.lastUpdated,
      lastUpdatedBy: lastUpdatedBy ?? this.lastUpdatedBy,
    );
  }

  factory CareTask.fromJson(Map<String, dynamic> json) =>
      _$CareTaskFromJson(json);

  Map<String, dynamic> toJson() => _$CareTaskToJson(this);

  @override
  List<Object?> get props => <Object?>[
        time,
        type,
        taskId,
        title,
        patientId,
        repeatDays,
        lastUpdated,
        description,
        lastUpdatedBy,
      ];

  @override
  bool get stringify => true;
}
