// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medication_task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicationTask _$MedicationTaskFromJson(Map<String, dynamic> json) =>
    MedicationTask(
      endDate: json['endDate'] == null
          ? null
          : DateTime.parse(json['endDate'] as String),
      startDate: json['startDate'] == null
          ? null
          : DateTime.parse(json['startDate'] as String),
      description: json['description'] as String?,
      dosage: json['dosage'] as num,
      metric: json['metric'] as String,
      frequency: json['frequency'] as String,
      title: json['title'] as String,
      time: DateTime.parse(json['time'] as String),
      taskId: json['taskId'] as String,
      patientId: json['patientId'] as String,
      type: $enumDecode(_$CareTaskTypeEnumMap, json['type']),
      lastUpdated: DateTime.parse(json['lastUpdated'] as String),
      lastUpdatedBy: json['lastUpdatedBy'] as String,
      repeatDays: (json['repeatDays'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$MedicationTaskToJson(MedicationTask instance) =>
    <String, dynamic>{
      'title': instance.title,
      'taskId': instance.taskId,
      'time': instance.time.toIso8601String(),
      'patientId': instance.patientId,
      'type': _$CareTaskTypeEnumMap[instance.type]!,
      'description': instance.description,
      'lastUpdated': instance.lastUpdated.toIso8601String(),
      'lastUpdatedBy': instance.lastUpdatedBy,
      'repeatDays': instance.repeatDays,
      'dosage': instance.dosage,
      'metric': instance.metric,
      'frequency': instance.frequency,
      'startDate': instance.startDate?.toIso8601String(),
      'endDate': instance.endDate?.toIso8601String(),
    };

const _$CareTaskTypeEnumMap = {
  CareTaskType.general: 'general',
  CareTaskType.meal: 'meal',
  CareTaskType.medication: 'medication',
};
