import 'package:json_annotation/json_annotation.dart';

import '../../../enums/enums.dart';
import '../care_task/care_task.dart';

part 'medication_task.g.dart';

@JsonSerializable()
class MedicationTask extends CareTask {
  final num dosage;
  final String metric;
  final String frequency;
  final DateTime? startDate;
  final DateTime? endDate;

  const MedicationTask({
    this.endDate,
    this.startDate,
    String? description,
    required this.dosage,
    required this.metric,
    required this.frequency,
    required String title,
    required DateTime time,
    required String taskId,
    required String patientId,
    required CareTaskType type,
    required DateTime lastUpdated,
    required String lastUpdatedBy,
    required List<String> repeatDays,
  }) : super(
          type: type,
          time: time,
          title: title,
          taskId: taskId,
          patientId: patientId,
          repeatDays: repeatDays,
          description: description,
          lastUpdated: lastUpdated,
          lastUpdatedBy: lastUpdatedBy,
        );

  @override
  MedicationTask copyWith({
    num? dosage,
    String? title,
    String? taskId,
    DateTime? time,
    String? metric,
    String? frequency,
    String? patientId,
    DateTime? endDate,
    DateTime? startDate,
    CareTaskType? type,
    String? description,
    DateTime? lastUpdated,
    String? lastUpdatedBy,
    List<String>? repeatDays,
  }) {
    return MedicationTask(
      time: time ?? this.time,
      type: type ?? this.type,
      title: title ?? this.title,
      dosage: dosage ?? this.dosage,
      taskId: taskId ?? this.taskId,
      metric: metric ?? this.metric,
      endDate: endDate ?? this.endDate,
      startDate: startDate ?? this.startDate,
      frequency: frequency ?? this.frequency,
      patientId: patientId ?? this.patientId,
      repeatDays: repeatDays ?? this.repeatDays,
      description: description ?? this.description,
      lastUpdated: lastUpdated ?? this.lastUpdated,
      lastUpdatedBy: lastUpdatedBy ?? this.lastUpdatedBy,
    );
  }

  factory MedicationTask.fromJson(Map<String, dynamic> json) =>
      _$MedicationTaskFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$MedicationTaskToJson(this);

  @override
  List<Object?> get props => <Object?>[
        time,
        type,
        title,
        taskId,
        dosage,
        metric,
        endDate,
        startDate,
        frequency,
        patientId,
        repeatDays,
        lastUpdated,
        description,
        lastUpdatedBy,
      ];

  @override
  bool get stringify => true;
}
