import 'package:equatable/equatable.dart';

class CircleMemberData extends Equatable {
  final String uid;
  final String email;
  final String relation;
  final bool isNextOfKin;
  final String? memberId;
  final String patientId;
  const CircleMemberData({
    this.memberId,
    required this.uid,
    required this.email,
    required this.relation,
    required this.patientId,
    required this.isNextOfKin,
  });

  CircleMemberData copyWith({
    String? uid,
    String? email,
    String? memberId,
    String? relation,
    bool? isNextOfKin,
    String? patientId,
  }) {
    return CircleMemberData(
      uid: uid ?? this.uid,
      email: email ?? this.email,
      memberId: memberId ?? this.memberId,
      relation: relation ?? this.relation,
      patientId: patientId ?? this.patientId,
      isNextOfKin: isNextOfKin ?? this.isNextOfKin,
    );
  }

  @override
  List<Object?> get props => <Object?>[
        uid,
        email,
        memberId,
        relation,
        patientId,
        isNextOfKin,
      ];
}
