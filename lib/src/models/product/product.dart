import 'package:in_app_purchase/in_app_purchase.dart';

import '../../enums/enums.dart';


class Product {
  final ProductStatus status;
  final ProductDetails productDetails;

  Product(this.productDetails) : status = ProductStatus.purchasable;

  String get id => productDetails.id;
  String get title => productDetails.title;
  String get description => productDetails.description;
  String get price => productDetails.price;
  String get subscriptionPeriod {
    switch (productDetails.id) {
      case 'casa_1199_1month':
        return 'Monthly';
      case 'casa_3299_3months':
        return 'Quarterly';
      case 'casa_6599_6months':
        return 'Yearly';
      default:
        return 'Unknown';
    }
  }
}
