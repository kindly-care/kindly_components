import 'package:hive/hive.dart';

part 'auth_details.g.dart';

@HiveType(typeId: 4)
class AuthDetails { 
  @HiveField(0)
  final String? name;
  @HiveField(1)
  final String? phone;
  @HiveField(2)
  final String email;
  @HiveField(3)
  final String password;

  const AuthDetails({
    this.name,
    this.phone,
    required this.email,
    required this.password,
  });

  AuthDetails copyWith({
    String? name,
    String? phone,
    String? email,
    String? password,
  }) {
    return AuthDetails(
      name: name ?? this.name,
      email: email ?? this.email,
      phone: phone ?? this.phone,
      password: password ?? this.password,
    );
  }
}
