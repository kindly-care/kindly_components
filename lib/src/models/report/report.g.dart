// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ReportAdapter extends TypeAdapter<Report> {
  @override
  final int typeId = 0;

  @override
  Report read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Report(
      timelog: fields[3] as TimeLog?,
      summary: fields[11] as String?,
      painLocation: fields[15] as String?,
      bloodPressure: fields[16] as BloodPressureRecord?,
      bloodGlucose: fields[17] as BloodGlucoseRecord?,
      temperature: fields[18] as TemperatureRecord?,
      weight: fields[19] as WeightRecord?,
      mood: fields[0] as String?,
      reportId: fields[2] as String,
      patientId: fields[5] as String,
      createdAt: fields[8] as DateTime,
      patientName: fields[9] as String,
      dateCreated: fields[10] as String,
      careActions: (fields[13] as List).cast<CareAction>(),
      patientInPain: fields[14] as bool,
      careProviderId: fields[1] as String,
      attachedPhotos: (fields[12] as List).cast<String>(),
      careProviderName: fields[4] as String,
      careProviderPhone: fields[6] as String,
      careProviderAvatar: fields[7] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Report obj) {
    writer
      ..writeByte(20)
      ..writeByte(0)
      ..write(obj.mood)
      ..writeByte(1)
      ..write(obj.careProviderId)
      ..writeByte(2)
      ..write(obj.reportId)
      ..writeByte(3)
      ..write(obj.timelog)
      ..writeByte(4)
      ..write(obj.careProviderName)
      ..writeByte(5)
      ..write(obj.patientId)
      ..writeByte(6)
      ..write(obj.careProviderPhone)
      ..writeByte(7)
      ..write(obj.careProviderAvatar)
      ..writeByte(8)
      ..write(obj.createdAt)
      ..writeByte(9)
      ..write(obj.patientName)
      ..writeByte(10)
      ..write(obj.dateCreated)
      ..writeByte(11)
      ..write(obj.summary)
      ..writeByte(12)
      ..write(obj.attachedPhotos)
      ..writeByte(13)
      ..write(obj.careActions)
      ..writeByte(14)
      ..write(obj.patientInPain)
      ..writeByte(15)
      ..write(obj.painLocation)
      ..writeByte(16)
      ..write(obj.bloodPressure)
      ..writeByte(17)
      ..write(obj.bloodGlucose)
      ..writeByte(18)
      ..write(obj.temperature)
      ..writeByte(19)
      ..write(obj.weight);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ReportAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Report _$ReportFromJson(Map<String, dynamic> json) => Report(
      timelog: json['timelog'] == null
          ? null
          : TimeLog.fromJson(json['timelog'] as Map<String, dynamic>),
      summary: json['summary'] as String?,
      painLocation: json['painLocation'] as String?,
      bloodPressure: json['bloodPressure'] == null
          ? null
          : BloodPressureRecord.fromJson(
              json['bloodPressure'] as Map<String, dynamic>),
      bloodGlucose: json['bloodGlucose'] == null
          ? null
          : BloodGlucoseRecord.fromJson(
              json['bloodGlucose'] as Map<String, dynamic>),
      temperature: json['temperature'] == null
          ? null
          : TemperatureRecord.fromJson(
              json['temperature'] as Map<String, dynamic>),
      weight: json['weight'] == null
          ? null
          : WeightRecord.fromJson(json['weight'] as Map<String, dynamic>),
      mood: json['mood'] as String?,
      reportId: json['reportId'] as String,
      patientId: json['patientId'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      patientName: json['patientName'] as String,
      dateCreated: json['dateCreated'] as String,
      careActions: (json['careActions'] as List<dynamic>)
          .map((e) => CareAction.fromJson(e as Map<String, dynamic>))
          .toList(),
      patientInPain: json['patientInPain'] as bool,
      careProviderId: json['careProviderId'] as String,
      attachedPhotos: (json['attachedPhotos'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      careProviderName: json['careProviderName'] as String,
      careProviderPhone: json['careProviderPhone'] as String,
      careProviderAvatar: json['careProviderAvatar'] as String,
    );

Map<String, dynamic> _$ReportToJson(Report instance) => <String, dynamic>{
      'mood': instance.mood,
      'careProviderId': instance.careProviderId,
      'reportId': instance.reportId,
      'timelog': instance.timelog?.toJson(),
      'careProviderName': instance.careProviderName,
      'patientId': instance.patientId,
      'careProviderPhone': instance.careProviderPhone,
      'careProviderAvatar': instance.careProviderAvatar,
      'createdAt': instance.createdAt.toIso8601String(),
      'patientName': instance.patientName,
      'dateCreated': instance.dateCreated,
      'summary': instance.summary,
      'attachedPhotos': instance.attachedPhotos,
      'careActions': instance.careActions.map((e) => e.toJson()).toList(),
      'patientInPain': instance.patientInPain,
      'painLocation': instance.painLocation,
      'bloodPressure': instance.bloodPressure?.toJson(),
      'bloodGlucose': instance.bloodGlucose?.toJson(),
      'temperature': instance.temperature?.toJson(),
      'weight': instance.weight?.toJson(),
    };
