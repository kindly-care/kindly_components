import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

import '../actions/actions.dart';
import '../records/records.dart';
import '../time_log/time_log.dart';

part 'report.g.dart';

@HiveType(typeId: 0)
@JsonSerializable(explicitToJson: true)
class Report {
  @HiveField(0)
  final String? mood;
  @HiveField(1)
  final String careProviderId;
  @HiveField(2)
  final String reportId;
  @HiveField(3)
  final TimeLog? timelog;
  @HiveField(4)
  final String careProviderName;
  @HiveField(5)
  final String patientId;
  @HiveField(6)
  final String careProviderPhone;
  @HiveField(7)
  final String careProviderAvatar;
  @HiveField(8)
  final DateTime createdAt;
  @HiveField(9)
  final String patientName;
  @HiveField(10)
  final String dateCreated;
  @HiveField(11)
  final String? summary;
  @HiveField(12)
  final List<String> attachedPhotos;
  @HiveField(13)
  final List<CareAction> careActions;
  @HiveField(14)
  final bool patientInPain;
  @HiveField(15)
  final String? painLocation;
  @HiveField(16)
  final BloodPressureRecord? bloodPressure;
  @HiveField(17)
  final BloodGlucoseRecord? bloodGlucose;
  @HiveField(18)
  final TemperatureRecord? temperature;
  @HiveField(19)
  final WeightRecord? weight;

  const Report({
    this.timelog,
    this.summary,
    this.painLocation,
    this.bloodPressure,
    this.bloodGlucose,
    this.temperature,
    this.weight,
    required this.mood,
    required this.reportId,
    required this.patientId,
    required this.createdAt,
    required this.patientName,
    required this.dateCreated,
    required this.careActions,
    required this.patientInPain,
    required this.careProviderId,
    required this.attachedPhotos,
    required this.careProviderName,
    required this.careProviderPhone,
    required this.careProviderAvatar,
  });

  Report copyWith({
    String? mood,
    String? careProviderId,
    String? summary,
    String? reportId,
    TimeLog? timelog,
    String? careProviderName,
    String? patientId,
    String? careProviderPhone,
    String? painLocation,
    String? careProviderAvatar,
    DateTime? createdAt,
    String? dateCreated,
    bool? patientInPain,
    String? patientName,
    WeightRecord? weight,
    List<String>? attachedPhotos,
    List<CareAction>? careActions,
    BloodGlucoseRecord? bloodGlucose,
    TemperatureRecord? temperature,
    BloodPressureRecord? bloodPressure,
  }) {
    return Report(
      mood: mood ?? this.mood,
      weight: weight ?? this.weight,
      timelog: timelog ?? this.timelog,
      summary: summary ?? this.summary,
      careProviderId: careProviderId ?? this.careProviderId,
      reportId: reportId ?? this.reportId,
      createdAt: createdAt ?? this.createdAt,
      careProviderName: careProviderName ?? this.careProviderName,
      patientId: patientId ?? this.patientId,
      bloodGlucose: bloodGlucose ?? this.bloodGlucose,
      careProviderPhone: careProviderPhone ?? this.careProviderPhone,
      careActions: careActions ?? this.careActions,
      careProviderAvatar: careProviderAvatar ?? this.careProviderAvatar,
      patientName: patientName ?? this.patientName,
      dateCreated: dateCreated ?? this.dateCreated,
      temperature: temperature ?? this.temperature,
      painLocation: painLocation ?? this.painLocation,
      patientInPain: patientInPain ?? this.patientInPain,
      bloodPressure: bloodPressure ?? this.bloodPressure,
      attachedPhotos: attachedPhotos ?? this.attachedPhotos,
    );
  }

  factory Report.fromJson(Map<String, dynamic> json) => _$ReportFromJson(json);

  Map<String, dynamic> toJson() => _$ReportToJson(this);
}
