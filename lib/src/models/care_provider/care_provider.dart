import 'dart:io';

import 'package:json_annotation/json_annotation.dart';

import '../../enums/enums.dart';
import '../address/address.dart';
import '../app_user/app_user.dart';
import '../assigned_patient/assigned_patient.dart';

part 'care_provider.g.dart';

@JsonSerializable(explicitToJson: true)
class CareProvider extends AppUser {
  final String bio;
  final double rating;
  final bool isVerified;
  final int? yearStarted;
  final bool isClockedIn;
  final List<String> images;
  final List<String> duties;
  final List<String> services;
  final bool hasDriverLicense;
  final bool isCovidVaccinated;
  final List<String> occupation;
  @JsonKey(ignore: true)
  final List<File>? localImages;
  final List<String> certificates;
  final Map<String, String> availability;
  final Map<String, AssignedPatient> assignedPatients;

  const CareProvider({
    DateTime? dob,
    String? email,
    String? gender,
    Address? address,
    this.rating = 0,
    this.yearStarted,
    this.isVerified = false,
    this.isClockedIn = false,
    UserType userType = UserType.careProvider,
    required this.bio,
    required String uid,
    required this.images,
    required this.duties,
    required String name,
    required String phone,
    required this.services,
    required String avatar,
    required this.occupation,
    required this.availability,
    required String streamToken,
    required this.certificates,
    required DateTime joinedDate,
    required String messageToken,
    required List<String> contacts,
    required this.hasDriverLicense,
    required this.assignedPatients,
    required this.isCovidVaccinated,
    File? localAvatar,
    this.localImages,
  }) : super(
          dob: dob,
          uid: uid,
          name: name,
          email: email,
          phone: phone,
          avatar: avatar,
          gender: gender,
          address: address,
          contacts: contacts,
          userType: userType,
          joinedDate: joinedDate,
          streamToken: streamToken,
          localAvatar: localAvatar,
          messageToken: messageToken,
        );

  @override
  CareProvider copyWith({
    String? bio,
    String? uid,
    String? name,
    DateTime? dob,
    String? phone,
    String? avatar,
    String? email,
    String? gender,
    double? rating,
    bool? isClockedIn,
    Address? address,
    bool? isVerified,
    File? localAvatar,
    UserType? userType,
    int? yearStarted,
    List<String>? duties,
    String? streamToken,
    List<String>? images,
    DateTime? joinedDate,
    String? messageToken,
    List<String>? contacts,
    bool? isCovidVaccinated,
    List<String>? services,
    bool? hasDriverLicense,
    List<File>? localImages,
    List<String>? occupation,
    List<String>? certificates,
    Map<String, String>? availability,
    Map<String, AssignedPatient>? assignedPatients,
  }) {
    return CareProvider(
      dob: dob ?? this.dob,
      uid: uid ?? this.uid,
      bio: bio ?? this.bio,
      name: name ?? this.name,
      email: email ?? this.email,
      phone: phone ?? this.phone,
      images: images ?? this.images,
      gender: gender ?? this.gender,
      avatar: avatar ?? this.avatar,
      rating: rating ?? this.rating,
      duties: duties ?? this.duties,
      address: address ?? this.address,
      contacts: contacts ?? this.contacts,
      userType: userType ?? this.userType,
      services: services ?? this.services,
      isVerified: isVerified ?? this.isVerified,
      occupation: occupation ?? this.occupation,
      joinedDate: joinedDate ?? this.joinedDate,
      localImages: localImages ?? this.localImages,
      isClockedIn: isClockedIn ?? this.isClockedIn,
      streamToken: streamToken ?? this.streamToken,
      yearStarted: yearStarted ?? this.yearStarted,
      localAvatar: localAvatar ?? this.localAvatar,
      certificates: certificates ?? this.certificates,
      messageToken: messageToken ?? this.messageToken,
      availability: availability ?? this.availability,
      assignedPatients: assignedPatients ?? this.assignedPatients,
      hasDriverLicense: hasDriverLicense ?? this.hasDriverLicense,
      isCovidVaccinated: isCovidVaccinated ?? this.isCovidVaccinated,
    );
  }

  factory CareProvider.fromJson(Map<String, dynamic> json) =>
      _$CareProviderFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$CareProviderToJson(this);

  String get sundayAvailability => availability['Sunday']!;
  String get mondayAvailability => availability['Monday']!;
  String get tuesdayAvailability => availability['Tuesday']!;
  String get wednesdayAvailability => availability['Wednesday']!;
  String get thursdayAvailability => availability['Thursday']!;
  String get fridayAvailability => availability['Friday']!;
  String get saturdayAvailability => availability['Saturday']!;
}
