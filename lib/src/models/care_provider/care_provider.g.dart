// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'care_provider.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CareProvider _$CareProviderFromJson(Map<String, dynamic> json) => CareProvider(
      dob: json['dob'] == null ? null : DateTime.parse(json['dob'] as String),
      email: json['email'] as String?,
      gender: json['gender'] as String?,
      address: json['address'] == null
          ? null
          : Address.fromJson(json['address'] as Map<String, dynamic>),
      rating: (json['rating'] as num?)?.toDouble() ?? 0,
      yearStarted: json['yearStarted'] as int?,
      isVerified: json['isVerified'] as bool? ?? false,
      isClockedIn: json['isClockedIn'] as bool? ?? false,
      userType: $enumDecodeNullable(_$UserTypeEnumMap, json['userType']) ??
          UserType.careProvider,
      bio: json['bio'] as String,
      uid: json['uid'] as String,
      images:
          (json['images'] as List<dynamic>).map((e) => e as String).toList(),
      duties:
          (json['duties'] as List<dynamic>).map((e) => e as String).toList(),
      name: json['name'] as String,
      phone: json['phone'] as String,
      services:
          (json['services'] as List<dynamic>).map((e) => e as String).toList(),
      avatar: json['avatar'] as String,
      occupation: (json['occupation'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      availability: Map<String, String>.from(json['availability'] as Map),
      streamToken: json['streamToken'] as String,
      certificates: (json['certificates'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      joinedDate: DateTime.parse(json['joinedDate'] as String),
      messageToken: json['messageToken'] as String,
      contacts:
          (json['contacts'] as List<dynamic>).map((e) => e as String).toList(),
      hasDriverLicense: json['hasDriverLicense'] as bool,
      assignedPatients: (json['assignedPatients'] as Map<String, dynamic>).map(
        (k, e) =>
            MapEntry(k, AssignedPatient.fromJson(e as Map<String, dynamic>)),
      ),
      isCovidVaccinated: json['isCovidVaccinated'] as bool,
    );

Map<String, dynamic> _$CareProviderToJson(CareProvider instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'phone': instance.phone,
      'avatar': instance.avatar,
      'gender': instance.gender,
      'email': instance.email,
      'dob': instance.dob?.toIso8601String(),
      'address': instance.address?.toJson(),
      'userType': _$UserTypeEnumMap[instance.userType]!,
      'streamToken': instance.streamToken,
      'joinedDate': instance.joinedDate.toIso8601String(),
      'messageToken': instance.messageToken,
      'contacts': instance.contacts,
      'bio': instance.bio,
      'rating': instance.rating,
      'isVerified': instance.isVerified,
      'yearStarted': instance.yearStarted,
      'isClockedIn': instance.isClockedIn,
      'images': instance.images,
      'duties': instance.duties,
      'services': instance.services,
      'hasDriverLicense': instance.hasDriverLicense,
      'isCovidVaccinated': instance.isCovidVaccinated,
      'occupation': instance.occupation,
      'certificates': instance.certificates,
      'availability': instance.availability,
      'assignedPatients':
          instance.assignedPatients.map((k, e) => MapEntry(k, e.toJson())),
    };

const _$UserTypeEnumMap = {
  UserType.user: 'user',
  UserType.patient: 'patient',
  UserType.careProvider: 'careProvider',
};
