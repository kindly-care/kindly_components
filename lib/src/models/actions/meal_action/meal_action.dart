import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../enums/enums.dart';
import '../care_action/care_action.dart';

part 'meal_action.g.dart';

@HiveType(typeId: 3)
@JsonSerializable()
class MealAction extends CareAction {
  @HiveField(11)
  final String meal;
  @HiveField(12)
  final String mealType;
  const MealAction({
    required this.meal,
    required String title,
    required this.mealType,
    required String taskId,
    required String? comment,
    required String actionId,
    required DateTime createdAt,
    required String dateCreated,
    required List<String> images,
    required CareTaskType taskType,
    required String careProviderId,
    required String patientId,
    required CareActionStatus status,
  }) : super(
          title: title,
          status: status,
          taskId: taskId,
          comment: comment,
          actionId: actionId,
          taskType: taskType,
          createdAt: createdAt,
          dateCreated: dateCreated,
          careProviderId: careProviderId,
          patientId: patientId,
        );

  @override
  MealAction copyWith({
    String? meal,
    String? title,
    String? taskId,
    String? comment,
    String? actionId,
    String? mealType,
    String? dateCreated,
    DateTime? createdAt,
    List<String>? images,
    String? careProviderId,
    CareTaskType? taskType,
    String? patientId,
    CareActionStatus? status,
  }) {
    return MealAction(
      meal: meal ?? this.meal,
      title: title ?? this.title,
      status: status ?? this.status,
      taskId: taskId ?? this.taskId,
      images: images ?? this.images,
      comment: comment ?? this.comment,
      mealType: mealType ?? this.mealType,
      actionId: actionId ?? this.actionId,
      taskType: taskType ?? this.taskType,
      createdAt: createdAt ?? this.createdAt,
      dateCreated: dateCreated ?? this.dateCreated,
      careProviderId: careProviderId ?? this.careProviderId,
      patientId: patientId ?? this.patientId,
    );
  }

  factory MealAction.fromJson(Map<String, dynamic> json) =>
      _$MealActionFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$MealActionToJson(this);

  @override
  List<Object?> get props => <Object?>[
        meal,
        title,
        status,
        comment,
        taskId,
        images,
        mealType,
        taskType,
        actionId,
        createdAt,
        dateCreated,
        careProviderId,
        patientId,
      ];

  @override
  bool get stringify => true;
}
