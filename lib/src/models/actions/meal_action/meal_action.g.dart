// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meal_action.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MealActionAdapter extends TypeAdapter<MealAction> {
  @override
  final int typeId = 3;

  @override
  MealAction read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MealAction(
      meal: fields[11] as String,
      title: fields[0] as String,
      mealType: fields[12] as String,
      taskId: fields[2] as String,
      comment: fields[4] as String?,
      actionId: fields[3] as String,
      createdAt: fields[6] as DateTime,
      dateCreated: fields[7] as String,
      images: (fields[8] as List).cast<String>(),
      taskType: fields[9] as CareTaskType,
      careProviderId: fields[1] as String,
      patientId: fields[5] as String,
      status: fields[10] as CareActionStatus,
    );
  }

  @override
  void write(BinaryWriter writer, MealAction obj) {
    writer
      ..writeByte(13)
      ..writeByte(11)
      ..write(obj.meal)
      ..writeByte(12)
      ..write(obj.mealType)
      ..writeByte(0)
      ..write(obj.title)
      ..writeByte(1)
      ..write(obj.careProviderId)
      ..writeByte(2)
      ..write(obj.taskId)
      ..writeByte(3)
      ..write(obj.actionId)
      ..writeByte(4)
      ..write(obj.comment)
      ..writeByte(5)
      ..write(obj.patientId)
      ..writeByte(6)
      ..write(obj.createdAt)
      ..writeByte(7)
      ..write(obj.dateCreated)
      ..writeByte(8)
      ..write(obj.images)
      ..writeByte(9)
      ..write(obj.taskType)
      ..writeByte(10)
      ..write(obj.status);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MealActionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MealAction _$MealActionFromJson(Map<String, dynamic> json) => MealAction(
      meal: json['meal'] as String,
      title: json['title'] as String,
      mealType: json['mealType'] as String,
      taskId: json['taskId'] as String,
      comment: json['comment'] as String?,
      actionId: json['actionId'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      dateCreated: json['dateCreated'] as String,
      images:
          (json['images'] as List<dynamic>).map((e) => e as String).toList(),
      taskType: $enumDecode(_$CareTaskTypeEnumMap, json['taskType']),
      careProviderId: json['careProviderId'] as String,
      patientId: json['patientId'] as String,
      status: $enumDecode(_$CareActionStatusEnumMap, json['status']),
    );

Map<String, dynamic> _$MealActionToJson(MealAction instance) =>
    <String, dynamic>{
      'title': instance.title,
      'careProviderId': instance.careProviderId,
      'taskId': instance.taskId,
      'actionId': instance.actionId,
      'comment': instance.comment,
      'patientId': instance.patientId,
      'createdAt': instance.createdAt.toIso8601String(),
      'dateCreated': instance.dateCreated,
      'images': instance.images,
      'taskType': _$CareTaskTypeEnumMap[instance.taskType]!,
      'status': _$CareActionStatusEnumMap[instance.status]!,
      'meal': instance.meal,
      'mealType': instance.mealType,
    };

const _$CareTaskTypeEnumMap = {
  CareTaskType.general: 'general',
  CareTaskType.meal: 'meal',
  CareTaskType.medication: 'medication',
};

const _$CareActionStatusEnumMap = {
  CareActionStatus.complete: 'complete',
  CareActionStatus.incomplete: 'incomplete',
  CareActionStatus.none: 'none',
};
