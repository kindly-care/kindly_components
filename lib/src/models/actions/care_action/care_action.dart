import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../enums/enums.dart';
import '../meal_action/meal_action.dart';
import '../medication_action/medication_action.dart';

part 'care_action.g.dart';

@HiveType(typeId: 2)
@JsonSerializable()
class CareAction extends Equatable {
  @HiveField(0)
  final String title;
  @HiveField(1)
  final String careProviderId;
  @HiveField(2)
  final String taskId;
  @HiveField(3)
  final String actionId;
  @HiveField(4)
  final String? comment;
  @HiveField(5)
  final String patientId;
  @HiveField(6)
  final DateTime createdAt;
  @HiveField(7)
  final String dateCreated;
  @HiveField(8)
  final List<String> images;
  @HiveField(9)
  final CareTaskType taskType;
  @HiveField(10)
  final CareActionStatus status;

  const CareAction({
    this.comment,
    required this.title,
    required this.status,
    required this.taskId,
    required this.careProviderId,
    required this.actionId,
    required this.taskType,
    required this.patientId,
    required this.createdAt,
    required this.dateCreated,
    this.images = const <String>[],
  });

  CareAction copyWith({
    String? title,
    String? taskId,
    String? comment,
    String? actionId,
    String? dateCreated,
    DateTime? createdAt,
    List<String>? images,
    CareTaskType? taskType,
    String? careProviderId,
    String? patientId,
    CareActionStatus? status,
  }) {
    return CareAction(
      title: title ?? this.title,
      status: status ?? this.status,
      taskId: taskId ?? this.taskId,
      images: images ?? this.images,
      comment: comment ?? this.comment,
      actionId: actionId ?? this.actionId,
      taskType: taskType ?? this.taskType,
      createdAt: createdAt ?? this.createdAt,
      dateCreated: dateCreated ?? this.dateCreated,
      careProviderId: careProviderId ?? this.careProviderId,
      patientId: patientId ?? this.patientId,
    );
  }

  factory CareAction.fromJson(Map<String, dynamic> json) {
    if (json['taskType'] == 'meal') {
      return MealAction.fromJson(json);
    } else if (json['taskType'] == 'medication') {
      return MedicationAction.fromJson(json);
    } else {
      return _$CareActionFromJson(json);
    }
  }

  Map<String, dynamic> toJson() {
    switch (taskType) {
      case CareTaskType.meal:
        final MealAction mealAction = this as MealAction;
        return mealAction.toJson();
      case CareTaskType.medication:
        final MedicationAction medAction = this as MedicationAction;
        return medAction.toJson();
      case CareTaskType.general:
        return _$CareActionToJson(this);
    }
  }

  @override
  List<Object?> get props => <Object?>[
        title,
        status,
        comment,
        taskId,
        images,
        taskType,
        actionId,
        createdAt,
        dateCreated,
        careProviderId,
        patientId,
      ];

  @override
  bool get stringify => true;
}
