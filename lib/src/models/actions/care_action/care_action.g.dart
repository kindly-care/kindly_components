// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'care_action.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CareActionAdapter extends TypeAdapter<CareAction> {
  @override
  final int typeId = 2;

  @override
  CareAction read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CareAction(
      comment: fields[4] as String?,
      title: fields[0] as String,
      status: fields[10] as CareActionStatus,
      taskId: fields[2] as String,
      careProviderId: fields[1] as String,
      actionId: fields[3] as String,
      taskType: fields[9] as CareTaskType,
      patientId: fields[5] as String,
      createdAt: fields[6] as DateTime,
      dateCreated: fields[7] as String,
      images: (fields[8] as List).cast<String>(),
    );
  }

  @override
  void write(BinaryWriter writer, CareAction obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.title)
      ..writeByte(1)
      ..write(obj.careProviderId)
      ..writeByte(2)
      ..write(obj.taskId)
      ..writeByte(3)
      ..write(obj.actionId)
      ..writeByte(4)
      ..write(obj.comment)
      ..writeByte(5)
      ..write(obj.patientId)
      ..writeByte(6)
      ..write(obj.createdAt)
      ..writeByte(7)
      ..write(obj.dateCreated)
      ..writeByte(8)
      ..write(obj.images)
      ..writeByte(9)
      ..write(obj.taskType)
      ..writeByte(10)
      ..write(obj.status);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CareActionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CareAction _$CareActionFromJson(Map<String, dynamic> json) => CareAction(
      comment: json['comment'] as String?,
      title: json['title'] as String,
      status: $enumDecode(_$CareActionStatusEnumMap, json['status']),
      taskId: json['taskId'] as String,
      careProviderId: json['careProviderId'] as String,
      actionId: json['actionId'] as String,
      taskType: $enumDecode(_$CareTaskTypeEnumMap, json['taskType']),
      patientId: json['patientId'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      dateCreated: json['dateCreated'] as String,
      images: (json['images'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const <String>[],
    );

Map<String, dynamic> _$CareActionToJson(CareAction instance) =>
    <String, dynamic>{
      'title': instance.title,
      'careProviderId': instance.careProviderId,
      'taskId': instance.taskId,
      'actionId': instance.actionId,
      'comment': instance.comment,
      'patientId': instance.patientId,
      'createdAt': instance.createdAt.toIso8601String(),
      'dateCreated': instance.dateCreated,
      'images': instance.images,
      'taskType': _$CareTaskTypeEnumMap[instance.taskType]!,
      'status': _$CareActionStatusEnumMap[instance.status]!,
    };

const _$CareActionStatusEnumMap = {
  CareActionStatus.complete: 'complete',
  CareActionStatus.incomplete: 'incomplete',
  CareActionStatus.none: 'none',
};

const _$CareTaskTypeEnumMap = {
  CareTaskType.general: 'general',
  CareTaskType.meal: 'meal',
  CareTaskType.medication: 'medication',
};
