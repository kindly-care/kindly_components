// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medication_action.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MedicationActionAdapter extends TypeAdapter<MedicationAction> {
  @override
  final int typeId = 7;

  @override
  MedicationAction read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MedicationAction(
      description: fields[15] as String?,
      dosage: fields[13] as num,
      metric: fields[14] as String,
      title: fields[0] as String,
      taskId: fields[2] as String,
      comment: fields[4] as String?,
      actionId: fields[3] as String,
      patientId: fields[5] as String,
      createdAt: fields[6] as DateTime,
      dateCreated: fields[7] as String,
      images: (fields[8] as List).cast<String>(),
      careProviderId: fields[1] as String,
      taskType: fields[9] as CareTaskType,
      status: fields[10] as CareActionStatus,
    );
  }

  @override
  void write(BinaryWriter writer, MedicationAction obj) {
    writer
      ..writeByte(14)
      ..writeByte(13)
      ..write(obj.dosage)
      ..writeByte(14)
      ..write(obj.metric)
      ..writeByte(15)
      ..write(obj.description)
      ..writeByte(0)
      ..write(obj.title)
      ..writeByte(1)
      ..write(obj.careProviderId)
      ..writeByte(2)
      ..write(obj.taskId)
      ..writeByte(3)
      ..write(obj.actionId)
      ..writeByte(4)
      ..write(obj.comment)
      ..writeByte(5)
      ..write(obj.patientId)
      ..writeByte(6)
      ..write(obj.createdAt)
      ..writeByte(7)
      ..write(obj.dateCreated)
      ..writeByte(8)
      ..write(obj.images)
      ..writeByte(9)
      ..write(obj.taskType)
      ..writeByte(10)
      ..write(obj.status);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MedicationActionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicationAction _$MedicationActionFromJson(Map<String, dynamic> json) =>
    MedicationAction(
      description: json['description'] as String?,
      dosage: json['dosage'] as num,
      metric: json['metric'] as String,
      title: json['title'] as String,
      taskId: json['taskId'] as String,
      comment: json['comment'] as String?,
      actionId: json['actionId'] as String,
      patientId: json['patientId'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      dateCreated: json['dateCreated'] as String,
      images:
          (json['images'] as List<dynamic>).map((e) => e as String).toList(),
      careProviderId: json['careProviderId'] as String,
      taskType: $enumDecode(_$CareTaskTypeEnumMap, json['taskType']),
      status: $enumDecode(_$CareActionStatusEnumMap, json['status']),
    );

Map<String, dynamic> _$MedicationActionToJson(MedicationAction instance) =>
    <String, dynamic>{
      'title': instance.title,
      'careProviderId': instance.careProviderId,
      'taskId': instance.taskId,
      'actionId': instance.actionId,
      'comment': instance.comment,
      'patientId': instance.patientId,
      'createdAt': instance.createdAt.toIso8601String(),
      'dateCreated': instance.dateCreated,
      'images': instance.images,
      'taskType': _$CareTaskTypeEnumMap[instance.taskType]!,
      'status': _$CareActionStatusEnumMap[instance.status]!,
      'dosage': instance.dosage,
      'metric': instance.metric,
      'description': instance.description,
    };

const _$CareTaskTypeEnumMap = {
  CareTaskType.general: 'general',
  CareTaskType.meal: 'meal',
  CareTaskType.medication: 'medication',
};

const _$CareActionStatusEnumMap = {
  CareActionStatus.complete: 'complete',
  CareActionStatus.incomplete: 'incomplete',
  CareActionStatus.none: 'none',
};
