import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../enums/enums.dart';
import '../care_action/care_action.dart';

part 'medication_action.g.dart';

@HiveType(typeId: 7)
@JsonSerializable()
class MedicationAction extends CareAction {
  @HiveField(13)
  final num dosage;
  @HiveField(14)
  final String metric;
  @HiveField(15)
  final String? description;
  const MedicationAction({
    this.description,
    required this.dosage,
    required this.metric,
    required String title,
    required String taskId,
    required String? comment,
    required String actionId,
    required String patientId,
    required DateTime createdAt,
    required String dateCreated,
    required List<String> images,
    required String careProviderId,
    required CareTaskType taskType,
    required CareActionStatus status,
  }) : super(
          title: title,
          status: status,
          taskId: taskId,
          comment: comment,
          actionId: actionId,
          taskType: taskType,
          createdAt: createdAt,
          dateCreated: dateCreated,
          careProviderId: careProviderId,
          patientId: patientId,
        );

  @override
  MedicationAction copyWith({
    num? dosage,
    String? title,
    String? metric,
    String? taskId,
    String? comment,
    String? actionId,
    String? description,
    String? dateCreated,
    DateTime? createdAt,
    List<String>? images,
    String? careProviderId,
    CareTaskType? taskType,
    String? patientId,
    CareActionStatus? status,
  }) {
    return MedicationAction(
      title: title ?? this.title,
      metric: metric ?? this.metric,
      dosage: dosage ?? this.dosage,
      status: status ?? this.status,
      taskId: taskId ?? this.taskId,
      images: images ?? this.images,
      comment: comment ?? this.comment,
      actionId: actionId ?? this.actionId,
      taskType: taskType ?? this.taskType,
      createdAt: createdAt ?? this.createdAt,
      description: description ?? this.description,
      dateCreated: dateCreated ?? this.dateCreated,
      careProviderId: careProviderId ?? this.careProviderId,
      patientId: patientId ?? this.patientId,
    );
  }

  factory MedicationAction.fromJson(Map<String, dynamic> json) =>
      _$MedicationActionFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$MedicationActionToJson(this);

  @override
  List<Object?> get props => <Object?>[
        title,
        status,
        comment,
        taskId,
        dosage,
        metric,
        images,
        taskType,
        actionId,
        createdAt,
        description,
        dateCreated,
        careProviderId,
        patientId,
      ];

  @override
  bool get stringify => true;
}
