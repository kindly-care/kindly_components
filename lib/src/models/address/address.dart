import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'address.g.dart';

@JsonSerializable()
class Address extends Equatable {
  final int number;
  final String city;
  final String suburb;
  final double? latitude;
  final double? longitude;
  final String streetName;

  const Address({
    this.latitude,
    this.longitude,
    required this.city,
    required this.suburb,
    required this.streetName,
    required this.number,
  });

  Address copyWith({
    int? number,
    String? city,
    String? suburb,
    double? latitude,
    double? longitude,
    String? streetName,
  }) {
    return Address(
      city: city ?? this.city,
      number: number ?? this.number,
      suburb: suburb ?? this.suburb,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      streetName: streetName ?? this.streetName,
    );
  }

  String fullAddress() {
    return '$number $streetName, $suburb, $city';
  }

  String streetAddress() {
    return '$number $streetName,';
  }

  String townAddress() => '$suburb, $city';

  String shortAddress() => '$suburb, $city';

  factory Address.fromJson(Map<String, dynamic> json) =>
      _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);

  @override
  List<Object?> get props =>
      <Object?>[city, suburb, latitude, longitude, number, streetName];

  @override
  bool get stringify => true;
}
