// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map<String, dynamic> json) => Address(
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      city: json['city'] as String,
      suburb: json['suburb'] as String,
      streetName: json['streetName'] as String,
      number: json['number'] as int,
    );

Map<String, dynamic> _$AddressToJson(Address instance) => <String, dynamic>{
      'number': instance.number,
      'city': instance.city,
      'suburb': instance.suburb,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'streetName': instance.streetName,
    };
