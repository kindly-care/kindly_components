// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Review _$ReviewFromJson(Map<String, dynamic> json) => Review(
      name: json['name'] as String,
      text: json['text'] as String,
      phone: json['phone'] as String,
      email: json['email'] as String?,
      rating: (json['rating'] as num).toDouble(),
      authorId: json['authorId'] as String,
      careProviderId: json['careProviderId'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
    );

Map<String, dynamic> _$ReviewToJson(Review instance) => <String, dynamic>{
      'name': instance.name,
      'text': instance.text,
      'phone': instance.phone,
      'rating': instance.rating,
      'email': instance.email,
      'careProviderId': instance.careProviderId,
      'authorId': instance.authorId,
      'createdAt': instance.createdAt.toIso8601String(),
    };
