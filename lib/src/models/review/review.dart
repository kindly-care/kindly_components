import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'review.g.dart';

@JsonSerializable()
class Review extends Equatable {
  final String name;
  final String text;
  final String phone;
  final double rating;
  final String? email;
  final String careProviderId;
  final String authorId;
  final DateTime createdAt;
  const Review({
    required this.name,
    required this.text,
    required this.phone,
    required this.email,
    required this.rating,
    required this.authorId,
    required this.careProviderId,
    required this.createdAt,
  });

  Review copyWith({
    String? name,
    String? text,
    String? phone,
    String? email,
    double? rating,
    String? careProviderId,
    String? authorId,
    DateTime? createdAt,
  }) {
    return Review(
      name: name ?? this.name,
      text: text ?? this.text,
      phone: phone ?? this.phone,
      email: email ?? this.email,
      rating: rating ?? this.rating,
      careProviderId: careProviderId ?? this.careProviderId,
      authorId: authorId ?? this.authorId,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  factory Review.fromJson(Map<String, dynamic> json) => _$ReviewFromJson(json);

  Map<String, dynamic> toJson() => _$ReviewToJson(this);

  @override
  List<Object?> get props => <Object?>[
        name,
        text,
        phone,
        email,
        careProviderId,
        createdAt,
        rating,
        authorId,
      ];

  @override
  bool get stringify => true;
}
