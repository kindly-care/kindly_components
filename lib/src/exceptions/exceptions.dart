class UpdateDataException implements Exception {
  final String _message;
  final bool retryOption;
  UpdateDataException(this._message, {this.retryOption = true});

  @override
  String toString() => _message;
}

class FetchDataException implements Exception {
  final String _message;
  FetchDataException(this._message);

  @override
  String toString() => _message;
}

class AuthException implements Exception {
  final String _message;
  AuthException(this._message);

  @override
  String toString() => _message;
}

class ImageException implements Exception {
  final String _message;
  ImageException(this._message);

  @override
  String toString() => _message;
}

class LocationException implements Exception {
  final String _message;
  LocationException(this._message);

  @override
  String toString() => _message;
}
