// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'enums.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CareTaskTypeAdapter extends TypeAdapter<CareTaskType> {
  @override
  final int typeId = 6;

  @override
  CareTaskType read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return CareTaskType.general;
      case 1:
        return CareTaskType.meal;
      case 2:
        return CareTaskType.medication;
      default:
        return CareTaskType.general;
    }
  }

  @override
  void write(BinaryWriter writer, CareTaskType obj) {
    switch (obj) {
      case CareTaskType.general:
        writer.writeByte(0);
        break;
      case CareTaskType.meal:
        writer.writeByte(1);
        break;
      case CareTaskType.medication:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CareTaskTypeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class CareActionStatusAdapter extends TypeAdapter<CareActionStatus> {
  @override
  final int typeId = 8;

  @override
  CareActionStatus read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return CareActionStatus.complete;
      case 1:
        return CareActionStatus.incomplete;
      case 2:
        return CareActionStatus.none;
      default:
        return CareActionStatus.complete;
    }
  }

  @override
  void write(BinaryWriter writer, CareActionStatus obj) {
    switch (obj) {
      case CareActionStatus.complete:
        writer.writeByte(0);
        break;
      case CareActionStatus.incomplete:
        writer.writeByte(1);
        break;
      case CareActionStatus.none:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CareActionStatusAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
