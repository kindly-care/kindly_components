import 'package:hive/hive.dart';
part 'enums.g.dart';

enum UserType { user, patient, careProvider }

enum NotificationType {
  info,
  report,
  interviewRequest,
  patientAssignment,
}

enum PaymentPeriod { weekly, biWeekly, monthly }

enum NoteType { note, reminder, task }

enum ErrorType { info, error, none }

enum ProductStatus { purchasable, purchased, pending }

@HiveType(typeId: 6)
enum CareTaskType {
  @HiveField(0)
  general,
  @HiveField(1)
  meal,
  @HiveField(2)
  medication
}

@HiveType(typeId: 8)
enum CareActionStatus {
  @HiveField(0)
  complete,
  @HiveField(1)
  incomplete,
  @HiveField(2)
  none,
}

enum ToastBrightness { light, dark }

enum DestinationPage { records, timelogs, subscriptions }
