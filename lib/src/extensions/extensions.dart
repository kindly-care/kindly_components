import 'dart:developer' as devtools show log;

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Testa {}

extension SimpleValidator on String? {
  String? get simpleValidate {
    if (this == null) {
      return 'Field required.';
    } else if (this!.isEmpty) {
      return 'Field required.';
    } else {
      return null;
    }
  }

  String? get simpleValidatePassword {
    if (this == null) {
      return 'Password required.';
    }
    if (this!.isEmpty) {
      return 'Password required';
    } else if (this!.length < 6) {
      return 'Password too short';
    } else {
      return null;
    }
  }

  String? get simpleValidateEmail {
    if (this == null) {
      return 'Field required.';
    } else if (this!.isEmpty) {
      return 'Field required.';
    } else if (!this!.isValidEmail()) {
      return 'Email not valid';
    } else {
      return null;
    }
  }

  String? get simpleValidatePhone {
    if (this == null) {
      return 'Phone number required';
    }
    if (this!.isEmpty) {
      return 'Phone number required';
    } else if (num.tryParse(this!) == null) {
      return 'Enter numerical figures only';
    } else {
      return null;
    }
  }

  String? get simpleValidateNumber {
    if (this == null) {
      return 'Field required.';
    } else if (this!.isEmpty) {
      return 'Field required.';
    } else if (num.tryParse(this!) == null) {
      return 'Numerical value required.';
    } else {
      return null;
    }
  }

  String? get simpleValidateYear {
    if (this == null) {
      return 'Field required.';
    } else if (this!.isEmpty) {
      return 'Field required.';
    } else if (num.tryParse(this!) == null) {
      return 'Numerical value required.';
    } else if (this!.length != 4) {
      return 'Valid year required.';
    } else {
      return null;
    }
  }
}

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$')
        .hasMatch(this);
  }
}

extension CapitalizeExtension on String {
  String? get capitalizeFirst => toBeginningOfSentenceCase(this);
  String get capitalizeAll => toUpperCase();
  String get capitalizeFirstofEach => trim()
      .split(' ')
      .map((String str) => toBeginningOfSentenceCase(str))
      .join(' ');
}

extension NameExtension on String {
  /// example: Thomas Mabika becomes Thomas
  String firstName() {
    return split(' ').first;
  }

  /// example: Thomas Mabika becomes Thomas M
  String firstNameLastInitial() {
    final List<String> splits = split(' ');
    if (splits.length == 1) {
      return splits.first;
    } else {
      return '${split(' ').first} ${split(' ').last[0]}';
    }
  }

  /// example: Thomas Mabika becomes T. Mabika
  String lastNameFirstInitial() {
    final List<String> splits = split(' ');
    if (splits.length == 1) {
      return splits.first;
    } else {
      return '${split(' ').first[0]}. ${split(' ').last}';
    }
  }
}

extension DistinctExtension<T> on Iterable<T> {
  Iterable<T> distinct<TKey>([TKey Function(T)? keySelector]) sync* {
    keySelector ??= (T v) => v as TKey;
    final Set<TKey> set = <TKey>{};

    for (final T t in this) {
      if (set.add(keySelector(t))) {
        yield t;
      }
    }
  }
}

extension LogX on Object {
  void log() => devtools.log(toString());
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
