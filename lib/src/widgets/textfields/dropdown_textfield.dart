import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class DropdownTextField extends StatelessWidget {
  final bool readOnly;
  final String? hintText;
  final String? labelText;
  final List<String> items;
  final String? helperText;
  final FocusNode? focusNode;
  final Function(String)? onChanged;
  final TextEditingController? controller;
  final String? Function(String? value)? validator;

  const DropdownTextField({
    super.key,
    this.hintText,
    this.onChanged,
    this.validator,
    this.labelText,
    this.controller,
    this.focusNode,
    this.helperText,
    required this.items,
    this.readOnly = false,
  });

  List<DropdownMenuItem<String>> _getDropdownItems() {
    return items
        .map(
          (String val) => DropdownMenuItem<String>(
            value: val,
            child: Text(val),
          ),
        )
        .toList();
  }

  String? _initValue() {
    final String? text = controller?.text;
    if (text != null && text.isNotEmpty) {
      return text;
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 1.0.w),
          child: Visibility(
            visible: labelText != null,
            child: Text(labelText ?? '', style: theme.bodyMedium),
          ),
        ),
        Visibility(
          visible: labelText != null,
          child: SizedBox(height: 0.6.h),
        ),
        DropdownButtonFormField<String>(
          value: _initValue(),
          focusNode: focusNode,
          validator: validator,
          hint: Text(
            hintText ?? 'Select',
            style: theme.bodyMedium,
          ),
          items: _getDropdownItems(),
          style: theme.bodyLarge?.copyWith(fontWeight: FontWeight.w500),
          onChanged: (String? val) {
            controller?.text = val!;
            if (onChanged != null) {
              onChanged!(val!);
            }
          },
          decoration: InputDecoration(
            filled: true,
            hintText: hintText,
            helperText: helperText,
            errorStyle:
                theme.labelLarge?.copyWith(color: Theme.of(context).errorColor),
            helperStyle: theme.labelMedium,
            hintStyle: theme.bodyMedium?.copyWith(color: Colors.grey),
            contentPadding:
                EdgeInsets.symmetric(vertical: 1.2.h, horizontal: 2.5.w),
            fillColor: Colors.white,
            enabledBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: Colors.grey.shade300),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: Theme.of(context).primaryColor),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: Theme.of(context).errorColor),
            ),
          ),
        ),
      ],
    );
  }
}
