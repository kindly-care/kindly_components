import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class MaterialTextField extends StatelessWidget {
  final bool readOnly;
  final int? maxLines;
  final int? minLines;
  final bool? enabled;
  final bool isOptional;
  final String? hintText;
  final Icon? prefixIcon;
  final String? labelText;
  final bool obscureText;
  final Widget? suffixIcon;
  final String? helperText;
  final VoidCallback? onTap;
  final FocusNode? focusNode;
  final Function(String)? onChanged;
  final TextInputType? textInputType;
  final Function()? onEditingComplete;
  final bool enableInteractiveSelection;
  final TextInputAction? textInputAction;
  final TextEditingController? controller;
  final ValueChanged<String>? onFieldSubmitted;

  final List<TextInputFormatter>? inputFormatters;
  final String? Function(String? value)? validator;

  const MaterialTextField({
    super.key,
    this.onTap,
    this.hintText,
    this.onChanged,
    this.suffixIcon,
    this.validator,
    this.controller,
    this.labelText,
    this.prefixIcon,
    this.maxLines,
    this.enabled,
    this.minLines,
    this.focusNode,
    this.helperText,
    this.textInputType,
    this.inputFormatters,
    this.readOnly = false,
    this.onFieldSubmitted,
    this.onEditingComplete,
    this.isOptional = false,
    this.obscureText = false,
    this.enableInteractiveSelection = true,
    this.textInputAction = TextInputAction.next,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 1.0.w),
          child: Row(
            children: <Widget>[
              Visibility(
                visible: labelText != null,
                child: Text(labelText ?? '', style: theme.bodyMedium),
              ),
              Visibility(
                visible: isOptional,
                child: Text(
                  ' (optional)',
                  style: theme.bodyMedium
                      ?.copyWith(color: Colors.black.withOpacity(0.4)),
                ),
              ),
            ],
          ),
        ),
        Visibility(
          visible: labelText != null,
          child: SizedBox(height: 0.6.h),
        ),
        TextFormField(
          onTap: onTap,
          enabled: enabled,
          maxLines: maxLines,
          minLines: minLines,
          onChanged: onChanged,
          validator: validator,
          focusNode: focusNode,
          controller: controller,
          obscureText: obscureText,
          keyboardType: textInputType,
          textInputAction: textInputAction,
          inputFormatters: inputFormatters,
          onFieldSubmitted: onFieldSubmitted,
          onEditingComplete: onEditingComplete,
          style: theme.bodyLarge?.copyWith(fontWeight: FontWeight.w500),
          decoration: InputDecoration(
            filled: true,
            hintText: hintText,
            suffixIcon: suffixIcon,
            helperText: helperText,
            errorStyle:
                theme.labelLarge?.copyWith(color: Theme.of(context).errorColor),
            prefixIcon: prefixIcon,
            helperStyle: theme.labelMedium,
            hintStyle: theme.bodyMedium?.copyWith(color: Colors.grey),
            contentPadding:
                EdgeInsets.symmetric(vertical: 2.2.h, horizontal: 2.5.w),
            fillColor: Colors.white,
            enabledBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: Colors.grey.shade300),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: Theme.of(context).primaryColor),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: Theme.of(context).errorColor),
            ),
          ),
          cursorColor: Theme.of(context).primaryColor,
          enableInteractiveSelection: enableInteractiveSelection,
        ),
      ],
    );
  }
}
