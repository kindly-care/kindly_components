import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class NoDataWidget extends StatelessWidget {
  final String message;
  const NoDataWidget(this.message, {super.key});

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.01),
      child: Center(
        child: Text(
          message,
          textAlign: TextAlign.center,
          style: Theme.of(context)
              .textTheme
              .bodyLarge
              ?.copyWith(fontSize: 17.5.sp),
        ),
      ),
    );
  }
}
