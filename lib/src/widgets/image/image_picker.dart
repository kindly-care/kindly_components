import 'dart:io';

import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../kindly_components.dart';

class ImagePicker extends StatefulWidget {
  final List<File>? selectedImages;
  final Function(List<File> file) onDone;
  const ImagePicker({super.key, required this.onDone, this.selectedImages});

  @override
  ImagePickerState createState() => ImagePickerState();
}

class ImagePickerState extends State<ImagePicker> {
  List<File> _files = <File>[];
  final double _space = 10.0;

  @override
  void initState() {
    super.initState();
    _files = widget.selectedImages ?? <File>[];
  }

  void _onRemoveImage(File file) {
    setState(() => _files.remove(file));
  }

  List<Widget> _widgets() {
    final List<Widget> widgets = <Widget>[];
    for (final File file in _files) {
      widgets.add(_ImageItem(
        image: file,
        onRemoveImage: _onRemoveImage,
      ));
    }

    return widgets
      ..add(_AddImageButton(onPressed: () async {
        try {
          _files = await ImageService.getImages();
          setState(() {
            widget.onDone(_files);
          });
        } catch (e) {
          showToast(message: e.toString());
        }
      }));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 2.0.w),
      child: Wrap(
        direction: Axis.horizontal,
        spacing: _space,
        runSpacing: _space,
        children: _widgets(),
      ),
    );
  }
}

class _AddImageButton extends StatelessWidget {
  final VoidCallback onPressed;
  const _AddImageButton({Key? key, required this.onPressed}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Container(
      height: height * 0.16,
      width: height * 0.16,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black12,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: IconButton(
        icon: Icon(
          Icons.add,
          color: const Color(0xFF969799),
          size: height * 0.04,
        ),
        onPressed: onPressed,
      ),
    );
  }
}

class _ImageItem extends StatelessWidget {
  final File image;
  final Function(File) onRemoveImage;

  const _ImageItem({
    Key? key,
    required this.image,
    required this.onRemoveImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Stack(
      clipBehavior: Clip.antiAlias,
      children: <Widget>[
        ClipRRect(
            borderRadius: BorderRadius.circular(4.0),
            child: Image.file(
              image,
              height: height * 0.16,
              width: height * 0.16,
              gaplessPlayback: true,
              fit: BoxFit.cover,
              // quality: 100,
            )),
        Positioned(
          right: 0,
          top: 0,
          child: InkWell(
            borderRadius: BorderRadius.circular(999.0),
            child: Icon(
              Icons.cancel,
              color: const Color(0xFFD6D6D6),
              size: height * 0.025,
            ),
            onTap: () => onRemoveImage(image),
          ),
        )
      ],
    );
  }
}
