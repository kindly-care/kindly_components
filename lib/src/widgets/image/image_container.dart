// ignore_for_file: depend_on_referenced_packages

import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import 'image_gallery.dart';

class ImageContainer extends StatelessWidget {
  final List<String> imageURLs;
  const ImageContainer({super.key, required this.imageURLs});

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 2.8.w,
      runSpacing: 2.8.w,
      direction: Axis.horizontal,
      children: imageURLs.mapIndexed((int index, String imageUrl) {
        return GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              CupertinoPageRoute<void>(
                fullscreenDialog: true,
                builder: (_) =>
                    ImageGallery(imageURLs: imageURLs, currentIndex: index + 1),
              ),
            );
          },
          child: SizedBox(
            height: 18.h,
            width: 29.w,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(6.0),
              child: CachedNetworkImage(
                imageUrl: imageUrl,
                fit: BoxFit.cover,
                placeholder: (_, __) => const Image(
                  image: AssetImage('assets/images/loading3.gif'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        );
      }).toList(),
    );
  }
}
