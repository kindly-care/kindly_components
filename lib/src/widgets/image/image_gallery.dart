import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../widgets/widgets.dart';

class ImageGallery extends StatefulWidget {
  final List<String> imageURLs;
  final int? currentIndex;

  const ImageGallery(
      {super.key, required this.imageURLs, this.currentIndex = 1});

  @override
  ImageGalleryState createState() => ImageGalleryState();
}

class ImageGalleryState extends State<ImageGallery> {
  late int _currentIndex;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _currentIndex = widget.currentIndex! - 1;
    _pageController = PageController(initialPage: _currentIndex);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.black,
        statusBarIconBrightness: Brightness.light,
      ),
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          backgroundColor: Colors.black,
          leading: IconButton(
            icon: Icon(
              Icons.close,
              color: Colors.white,
              size: 4.h,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        body: Stack(
          children: <Widget>[
            PhotoViewGallery.builder(
              itemCount: widget.imageURLs.length,
              builder: (BuildContext context, int index) {
                final String image = widget.imageURLs[index];
                return PhotoViewGalleryPageOptions(
                  imageProvider: CachedNetworkImageProvider(image),
                  minScale: PhotoViewComputedScale.contained,
                  maxScale: PhotoViewComputedScale.covered * 1.8,
                );
              },
              scrollPhysics: const BouncingScrollPhysics(),
              pageController: _pageController,
              loadingBuilder: (_, __) => const LoadingIndicator(),
              onPageChanged: (int index) {
                setState(() {
                  _currentIndex = index;
                });
              },
            ),
            Positioned(
              left: 0.0,
              right: 0.0,
              bottom: height * 0.02,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: widget.imageURLs.map((String image) {
                  return Container(
                    width: height * 0.01,
                    height: height * 0.01,
                    margin: EdgeInsets.symmetric(
                        vertical: height * 0.012, horizontal: height * 0.004),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _currentIndex == widget.imageURLs.indexOf(image)
                          ? Colors.white
                          : Colors.grey[700],
                    ),
                  );
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
