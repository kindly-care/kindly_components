import 'package:flutter/material.dart';

class MenuElement extends StatelessWidget {
  final String title;
  final bool enabled;
  const MenuElement({super.key, required this.title, this.enabled = true});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Text(
      title,
      style: enabled
          ? theme.titleSmall?.copyWith(
              fontWeight: FontWeight.w600,
            )
          : theme.titleSmall?.copyWith(
              fontWeight: FontWeight.w600,
              color: Theme.of(context).disabledColor,
            ),
    );
  }
}
