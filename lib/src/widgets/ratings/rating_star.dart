import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class RatingStar extends StatelessWidget {
  final num rating;
  final bool leftStar;
  const RatingStar({super.key, required this.rating, this.leftStar = true});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return leftStar
        ? Row(
            children: <Widget>[
              Icon(
                Icons.star,
                color: const Color(0xFFFE7404),
                size: 2.2.h,
              ),
              SizedBox(width: 0.5.w),
              Text(
                rating.toStringAsFixed(1),
                style: theme.bodyMedium!.copyWith(
                  fontWeight: FontWeight.w600,
                  color: Colors.black.withOpacity(0.65),
                ),
              ),
            ],
          )
        : Row(
            children: <Widget>[
              Text(
                rating.toStringAsFixed(1),
                style: theme.bodyMedium?.copyWith(
                  fontWeight: FontWeight.w600,
                  color: Colors.black.withOpacity(0.65),
                ),
              ),
              SizedBox(width: 0.5.w),
              Icon(
                Icons.star,
                color: const Color(0xFFFE7404),
                size: 2.2.h,
              ),
            ],
          );
  }
}
