import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import 'rating_star.dart';

class RatingContainer extends StatelessWidget {
  final num rating;
  const RatingContainer({super.key, required this.rating});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 0.8.w, vertical: 0.2.h),
      decoration: BoxDecoration(
        color: Colors.teal.shade50,
        borderRadius: BorderRadius.circular(7.0),
      ),
      child: RatingStar(rating: rating, leftStar: false),
    );
  }
}
