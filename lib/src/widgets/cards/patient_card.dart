import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../kindly_components.dart';

class PatientCard extends StatelessWidget {
  final Patient patient;
  final VoidCallback onTap;
  final VoidCallback onLongPress;

  const PatientCard({
    super.key,
    required this.onTap,
    required this.patient,
    required this.onLongPress,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      onTap: onTap,
      onLongPress: onLongPress,
      contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
      leading: CircleAvatar(
        radius: 3.8.h,
        backgroundImage: CachedNetworkImageProvider(patient.avatar),
      ),
      title: Text(patient.name, style: theme.titleMedium),
      subtitle: Text(
        patient.address?.shortAddress() ?? kUnknownAddress,
        style: theme.titleSmall,
      ),
      trailing: Icon(Icons.chevron_right, size: 3.8.h),
    );
  }
}
