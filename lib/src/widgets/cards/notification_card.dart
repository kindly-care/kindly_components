import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class NotificationCard extends StatelessWidget {
  final VoidCallback onTap;
  final VoidCallback? onLongPress;
  final PushNotification notification;
  const NotificationCard({
    super.key,
    this.onLongPress,
    required this.onTap,
    required this.notification,
  });

  @override
  Widget build(BuildContext context) {
    final bool isSeen = notification.isSeen;
    final TextTheme theme = Theme.of(context).textTheme;
    final String time = _getNotificationDate(notification.createdAt);
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 0.8.h),
      child: ListTile(
        onTap: onTap,
        onLongPress: onLongPress,
        tileColor: Theme.of(context).cardColor,
        contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.5)),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 0.5.h),
            Padding(
              padding: EdgeInsets.only(bottom: 0.8.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    flex: 2,
                    child: Text(
                      notification.title,
                      overflow: TextOverflow.ellipsis,
                      style: theme.titleMedium?.copyWith(
                        fontWeight: isSeen ? FontWeight.w600 : FontWeight.bold,
                      ),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 0.5.w),
                      decoration: BoxDecoration(
                        color: Colors.teal.shade50,
                        borderRadius: BorderRadius.circular(4.5),
                      ),
                      child: Text(
                        time,
                        style: theme.labelLarge?.copyWith(
                          fontWeight:
                              isSeen ? FontWeight.w500 : FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        subtitle: Padding(
          padding: EdgeInsets.only(bottom: 0.8.h),
          child: Text(
            notification.text,
            style: theme.titleSmall?.copyWith(
              fontWeight: isSeen ? FontWeight.w500 : FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}

String _getNotificationDate(DateTime date) {
  final DateTime today = DateTime.now();

  const Duration oneWeek = Duration(days: 7);

  final Duration difference = today.difference(date);

  if (date.isToday) {
    return DateFormat.Hm().format(date);
  } else if (date.isYesterday) {
    return 'Yesterday';
  } else if (difference.compareTo(oneWeek) < 1) {
    switch (date.weekday) {
      case 1:
        return 'Mon';
      case 2:
        return 'Tue';
      case 3:
        return 'Wed';
      case 4:
        return 'Thu';
      case 5:
        return 'Fri';
      case 6:
        return 'Sat';
      case 7:
        return 'Sun';
    }
  } else if (date.year == today.year) {
    final String oldDate = DateFormat.yMd('en_GB').format(date);
    return oldDate;
  } else {
    return DateFormat.yMd('en_GB').format(date);
  }
  return '';
}
