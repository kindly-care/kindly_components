import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class CareTaskCard extends StatelessWidget {
  final CareTask task;
  final VoidCallback? onLongPress;
  const CareTaskCard(this.task, {super.key, this.onLongPress});

  String _getTaskTitle() {
    final CareTask careTask = task;
    if (careTask is MedicationTask) {
      return '${careTask.title} (${careTask.dosage} ${careTask.metric})';
    } else {
      return careTask.title;
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String time = DateFormat.Hm().format(task.time);

    final String date = DateFormat.yMd('en_GB').format(task.lastUpdated);
    return Card(
      elevation: 0.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(9.5)),
      child: ListTile(
        onTap: () => _onTaskTap(context, task),
        onLongPress: onLongPress,
        contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              _getTaskTitle(),
              overflow: TextOverflow.ellipsis,
              style: theme.titleMedium,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 0.5.w),
              decoration: BoxDecoration(
                color: Colors.teal.shade50,
                borderRadius: BorderRadius.circular(4.5),
              ),
              child: task is MedicationTask
                  ? const SizedBox.shrink()
                  : Text(time, style: theme.titleMedium),
            ),
          ],
        ),
        subtitle: Padding(
          padding: EdgeInsets.only(top: 1.0.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                task.description ?? 'No additional information given.',
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: theme.titleSmall!.copyWith(fontSize: 17.5.sp),
              ),
              SizedBox(height: 1.5.h),
              Text(
                'Updated by ${task.lastUpdatedBy} on $date',
                style: theme.labelMedium,
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onTaskTap(BuildContext context, CareTask task) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String updated = DateFormat.yMMMMd('en_GB').format(task.lastUpdated);
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
      ),
      builder: (_) {
        return SizedBox(
          height: 58.0.h,
          child: SingleChildScrollView(
            physics: const NeverScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(horizontal: 2.8.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 1.5.h),
                Center(
                  child: Text('Care Task', style: theme.titleLarge),
                ),
                SizedBox(height: 4.5.h),
                Text(
                  task.title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: theme.titleLarge,
                ),
                SizedBox(height: 1.5.h),
                Text(
                  task.description ?? 'No additional information provided',
                  maxLines: 2,
                  style: theme.bodyLarge,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(height: 5.0.h),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Task Type', style: theme.bodyLarge),
                    Text(
                      task.type.name.capitalizeFirst ?? 'Unknown',
                      style: theme.bodyLarge,
                    ),
                  ],
                ),
                SizedBox(height: 3.0.h),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Created By', style: theme.bodyLarge),
                    Text(task.lastUpdatedBy, style: theme.bodyLarge),
                  ],
                ),
                SizedBox(height: 3.0.h),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Last Updated', style: theme.bodyLarge),
                    Text(updated, style: theme.bodyLarge),
                  ],
                ),
                SizedBox(height: 8.0.h),
                Center(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 0.4,
                    ),
                    onPressed: () => Navigator.pop(context),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 1.8.h, horizontal: 35.5.w),
                      child: Text(
                        'Exit',
                        style: theme.bodyLarge?.copyWith(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
