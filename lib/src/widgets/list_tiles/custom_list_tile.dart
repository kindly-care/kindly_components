import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class CustomListTile extends StatelessWidget {
  final String title;
  final String? subtitle;
  final Widget? trailing;
  final IconData icon;
  final Color? textColor;
  final VoidCallback? onTap;

  const CustomListTile({
    super.key,
    this.subtitle,
    this.trailing,
    this.textColor,
    required this.icon,
    required this.title,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      dense: true,
      isThreeLine: true,
      leading: Icon(icon, size: 3.5.h, color: Colors.black.withOpacity(0.4)),
      title: Text(
        title,
        style: theme.titleMedium?.copyWith(
          fontSize: 17.5.sp,
          color: Colors.black.withOpacity(0.5),
        ),
      ),
      subtitle: Text(
        subtitle != null ? subtitle! : '',
        style: theme.titleSmall?.copyWith(
          fontSize: 17.5.sp,
          color: textColor ?? Colors.black.withOpacity(0.65),
        ),
      ),
      trailing: trailing,
      onTap: onTap,
    );
  }
}
