import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class TextListTile extends StatelessWidget {
  final String title;
  final String subtitle;
  final VoidCallback onTap;

  const TextListTile({
    super.key,
    required this.title,
    required this.subtitle,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      onTap: onTap,
      title: Text(title, style: theme.titleMedium),
      subtitle: Text(
        subtitle,
        style: theme.titleSmall?.copyWith(color: Colors.black.withOpacity(0.5)),
      ),
      trailing: Icon(Icons.chevron_right, size: 3.8.h),
    );
  }
}
