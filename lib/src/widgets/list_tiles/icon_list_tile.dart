import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class IconListTile extends StatelessWidget {
  final String title;
  final IconData icon;
  final String? subtitle;
  final bool showTrailing;
  final Color? leadingColor;
  final VoidCallback onTap;
  const IconListTile({
    super.key,
    this.subtitle,
    this.leadingColor,
    required this.icon,
    required this.title,
    required this.onTap,
    this.showTrailing = true,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      leading: CircleAvatar(
        backgroundColor: leadingColor ?? Colors.grey.shade200,
        radius: 3.0.h,
        child: Icon(
          icon,
          color: Colors.black.withOpacity(0.6),
          size: 3.8.h,
        ),
      ),
      title: Text(title, style: theme.titleMedium),
      subtitle:
          subtitle == null ? null : Text(subtitle!, style: theme.titleSmall),
      onTap: onTap,
      trailing: showTrailing ? Icon(Icons.chevron_right, size: 3.8.h) : null,
    );
  }
}
