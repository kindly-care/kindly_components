import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class TaskListTile extends StatelessWidget {
  final String title;
  final IconData icon;
  final VoidCallback onTap;
  const TaskListTile({
    super.key,
    required this.title,
    required this.icon,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      onTap: onTap,
      title: Text(title, style: theme.titleMedium),
      minLeadingWidth: 5.0.w,
      leading: Icon(icon, size: 3.8.h),
      contentPadding: EdgeInsets.symmetric(horizontal: 1.0.w),
    );
  }
}
