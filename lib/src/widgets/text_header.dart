import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class TextHeader extends StatelessWidget {
  final String text;
  const TextHeader(this.text, {super. key});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Padding(
      padding: EdgeInsets.only(left: 0.8.w),
      child: Text(
        text,
        style: theme.bodyLarge?.copyWith(
          fontSize: 18.5.sp,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
