import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class ColoredPill extends StatelessWidget {
  final String title;
  final EdgeInsetsGeometry? margin;
  const ColoredPill({super.key, this.margin, required this.title});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 2.8.w, vertical: 0.8.h),
      margin: margin ?? EdgeInsets.symmetric(horizontal: 1.0.w),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        color: Colors.teal.shade100,
      ),
      child: Text(
        title,
        style: theme.bodyMedium?.copyWith(
          fontWeight: FontWeight.w600,
          color: Colors.black.withOpacity(0.65),
        ),
      ),
    );
  }
}
