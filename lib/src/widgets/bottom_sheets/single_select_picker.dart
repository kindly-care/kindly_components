import 'package:flutter/material.dart';

void showSingleSelectPicker(
  BuildContext context, {
  required String title,
  required String labelText,
  required List<String> items,
  required TextEditingController controller,
}) {
  showModalBottomSheet<void>(
    context: context,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(13.0),
        topRight: Radius.circular(13.0),
      ),
    ),
    builder: (BuildContext context) {
      return _BottomSheet(
        title: title,
        items: items,
        labelText: labelText,
        onSelected: (String? val) {
          controller.text = val!;
        },
        controller: controller,
      );
    },
  );
}

class _BottomSheet extends StatefulWidget {
  final String? title;
  final String labelText;
  final List<String> items;
  final TextEditingController controller;
  final Function(String?)? onSelected;
  const _BottomSheet({
    Key? key,
    this.title,
    required this.items,
    required this.labelText,
    required this.controller,
    required this.onSelected,
  }) : super(key: key);

  @override
  _BottomSheetState createState() => _BottomSheetState();
}

class _BottomSheetState extends State<_BottomSheet> {
  late String? _groupValue;

  @override
  void initState() {
    super.initState();
    _groupValue = widget.controller.text;
  }

  void _onChanged(String? value) {
    setState(() => _groupValue = value);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    child: Text(
                      'Cancel',
                      style: theme.bodyLarge?.copyWith(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                  Text(
                    widget.title ?? widget.labelText,
                    style: theme.bodyLarge,
                  ),
                  TextButton(
                    child: Text(
                      'Apply',
                      style: theme.bodyLarge?.copyWith(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    onPressed: () {
                      if (_groupValue == null) {
                        Navigator.pop(context);
                      } else {
                        if (widget.onSelected != null) {
                          widget.onSelected!(_groupValue);
                        }
                        Navigator.pop(context);
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
          const Divider(height: 8.0),
          Expanded(
            flex: 6,
            child: ListView.builder(
              physics: const BouncingScrollPhysics(),
              itemCount: widget.items.length,
              itemBuilder: (BuildContext context, int index) {
                final String item = widget.items[index];
                return RadioListTile<String>(
                  title: Text(
                    item,
                    style: theme.titleMedium,
                  ),
                  value: item,
                  groupValue: _groupValue,
                  onChanged: _onChanged,
                  activeColor: Theme.of(context).primaryColor,
                  controlAffinity: ListTileControlAffinity.trailing,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
