import 'package:flutter/material.dart';

void showMultiSelectBottomSheet(
  BuildContext context, {
  required String title,
  required List<String> items,
  required TextEditingController controller,
  required Function(List<String>)? onSelectedItems,
}) {
  showModalBottomSheet<void>(
    context: context,
    isDismissible: false,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(12.0),
        topRight: Radius.circular(12.0),
      ),
    ),
    builder: (BuildContext context) {
      return _BottomSheetContent(
        items: items,
        title: title,
        controller: controller,
        onSelectedItems: onSelectedItems,
      );
    },
  );
}

class _BottomSheetContent extends StatefulWidget {
  final String title;
  final List<String> items;
  final Function(List<String>)? onSelectedItems;
  final TextEditingController controller;

  const _BottomSheetContent({
    Key? key,
    this.onSelectedItems,
    required this.items,
    required this.title,
    required this.controller,
  }) : super(key: key);
  @override
  _SlidingBottomSheetState createState() => _SlidingBottomSheetState();
}

class _SlidingBottomSheetState extends State<_BottomSheetContent> {
  List<String> _selected = <String>[];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.controller.text.isNotEmpty) {
      _selected = widget.controller.text.split(', ');
    }
  }

  void _onChanged(String source) {
    if (_selected.contains(source)) {
      _selected.remove(source);
    } else {
      _selected.add(source);
    }

    if (widget.onSelectedItems != null) {
      widget.onSelectedItems!(_selected);
    }

    widget.controller.text = _selected.join(', ');

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    child: Text(
                      'Cancel',
                      style: theme.textTheme.bodyLarge!.copyWith(
                        color: theme.primaryColor,
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                  Text(
                    widget.title,
                    style: theme.textTheme.bodyLarge,
                  ),
                  TextButton(
                    child: Text(
                      'Done',
                      style: theme.textTheme.bodyLarge?.copyWith(
                        color: theme.primaryColor,
                      ),
                    ),
                    onPressed: () {
                      widget.controller.text = _selected.join(', ');
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          ),
          const Divider(height: 8.0),
          Expanded(
            flex: 8,
            child: ListView.builder(
              physics: const BouncingScrollPhysics(),
              itemCount: widget.items.length,
              itemBuilder: (BuildContext context, int index) {
                final String source = widget.items[index];
                return CheckboxListTile(
                  activeColor: Theme.of(context).primaryColor,
                  value: _selected.contains(source),
                  title: Text(source),
                  onChanged: (_) => _onChanged(source),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
