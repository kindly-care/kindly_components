import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

void showSingleSelectBottomSheet(
  BuildContext context, {
  required String title,
  required List<String> items,
  required Function(String) onSelected,
}) {
  showModalBottomSheet<void>(
      context: context,
      isDismissible: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
      ),
      builder: (BuildContext context) {
        return _BottomSheetContent(
          items: items,
          title: title,
          onSelected: onSelected,
        );
      });
}

class _BottomSheetContent extends StatefulWidget {
  final String title;
  final List<String> items;
  final Function(String) onSelected;
  const _BottomSheetContent({
    Key? key,
    required this.title,
    required this.items,
    required this.onSelected,
  }) : super(key: key);

  @override
  SingleSelectBottomSheetState createState() => SingleSelectBottomSheetState();
}

class SingleSelectBottomSheetState extends State<_BottomSheetContent> {
  String? _selected;
  late List<String> _items;

  @override
  void initState() {
    super.initState();
    _items = widget.items;
  }

  void _onChanged(String? value) {
    setState(() => _selected = value);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    child: Text(
                      'Cancel',
                      style: theme.bodyLarge?.copyWith(
                        color: Theme.of(context).primaryColor,
                        fontSize: 17.8.sp,
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                  Text(
                    widget.title,
                    style: theme.bodyLarge?.copyWith(fontSize: 18.0.sp),
                  ),
                  TextButton(
                    child: Text(
                      'Ok',
                      style: theme.bodyLarge?.copyWith(
                        color: Theme.of(context).primaryColor,
                        fontSize: 17.8.sp,
                      ),
                    ),
                    onPressed: () {
                      if (_selected == null) {
                        Navigator.pop(context);
                      } else {
                        Navigator.pop(context);
                        widget.onSelected(_selected!);
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
          const Divider(height: 8.0),
          Expanded(
            flex: 6,
            child: ListView.builder(
              physics: const BouncingScrollPhysics(),
              itemCount: widget.items.length,
              itemBuilder: (BuildContext context, int index) {
                final String item = _items[index];
                return CheckboxListTile(
                  title: Text(item, style: theme.bodyText1),
                  value: _selected == item,
                  onChanged: (_) => _onChanged(item),
                  activeColor: Theme.of(context).primaryColor,
                  controlAffinity: ListTileControlAffinity.trailing,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
