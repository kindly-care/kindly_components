import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

void optionsBottomSheet({
  required String title,
  required double height,
  required BuildContext context,
  required List<Widget> options,
}) {
  final TextTheme theme = Theme.of(context).textTheme;
  showModalBottomSheet<void>(
    context: context,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(12.0),
        topRight: Radius.circular(12.0),
      ),
    ),
    builder: (BuildContext context) {
      return SizedBox(
        height: height,
        child: ListView(
          physics: const NeverScrollableScrollPhysics(),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 3.5.w, right: 1.5.h),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(title, style: theme.titleLarge),
                  IconButton(
                    onPressed: () => Navigator.pop(context),
                    icon: Icon(Icons.close_outlined, size: 3.8.h),
                  )
                ],
              ),
            ),
            SizedBox(height: 1.5.h),
            ...options,
          ],
        ),
      );
    },
  );
}
