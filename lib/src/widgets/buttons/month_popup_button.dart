import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class MonthPopupButton extends StatefulWidget {
  final IconData icon;
  final Function(int) onChanged;
  const MonthPopupButton(
      {super.key, required this.icon, required this.onChanged});

  @override
  MonthPopupButtonState createState() => MonthPopupButtonState();
}

class MonthPopupButtonState extends State<MonthPopupButton> {
  late String _groupValue;

  @override
  void initState() {
    super.initState();
    _groupValue = DateFormat.MMM('en_GB').format(DateTime.now());
  }

  void _onMonthChanged(String? month) {
    final int index = _months.indexWhere((String mon) => mon == month);
    Navigator.pop(context);
    widget.onChanged(index + 1);
    setState(() => _groupValue = month!);
  }

  final List<String> _months = <String>[
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final Color primaryColor = Theme.of(context).primaryColor;
    return PopupMenuButton<String>(
      icon:
          Icon(widget.icon, size: 3.8.h, color: Colors.black.withOpacity(0.6)),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      itemBuilder: (BuildContext context) {
        return _months.map((String month) {
          return PopupMenuItem<String>(
            child: RadioListTile<String>(
              controlAffinity: ListTileControlAffinity.trailing,
              activeColor: primaryColor,
              title: Text(month,
                  style:
                      theme.bodyMedium?.copyWith(fontWeight: FontWeight.w600)),
              value: month,
              groupValue: _groupValue,
              onChanged: _onMonthChanged,
            ),
          );
        }).toList();
      },
    );
  }
}
