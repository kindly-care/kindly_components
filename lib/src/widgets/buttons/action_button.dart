import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../loading/loading_indicator.dart';

class ActionButton extends StatelessWidget {
  final String title;
  final bool enabled;
  final bool isLoading;
  final VoidCallback onPressed;
  const ActionButton({
    super.key,
    required this.title,
    this.enabled = true,
    this.isLoading = false,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final TextStyle? textStyle = theme.labelMedium?.copyWith(
      color: Colors.white,
      fontSize: 18.1.sp,
      fontWeight: FontWeight.w500,
    );
    return ElevatedButton(
      style: ElevatedButton.styleFrom(elevation: 0.4),
      onPressed: enabled ? onPressed : null,
      child: _showLoading()
          ? Padding(
              padding: EdgeInsets.symmetric(vertical: 1.0.h),
              child: const LoadingWidgetButton(),
            )
          : Padding(
              padding: EdgeInsets.symmetric(vertical: 1.8.h),
              child: Text(title, style: textStyle),
            ),
    );
  }

  bool _showLoading() => enabled && isLoading;
}
