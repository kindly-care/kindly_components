import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class IconMenuItem extends StatelessWidget {
  final String title;
  final bool enabled;
  final IconData icon;
  const IconMenuItem({
    super.key,
    required this.title,
    required this.icon,
    this.enabled = true,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          title,
          style: theme.titleSmall?.copyWith(
            fontSize: 17.5.sp,
            fontWeight: FontWeight.w600,
            color: enabled
                ? Colors.black.withOpacity(0.65)
                : Theme.of(context).disabledColor,
          ),
        ),
        SizedBox(width: 1.0.w),
        Icon(
          icon,
          color: enabled
              ? Colors.black.withOpacity(0.65)
              : Theme.of(context).disabledColor,
        ),
      ],
    );
  }
}
