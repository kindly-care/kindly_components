import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class BorderAvatar extends StatelessWidget {
  final double radius;
  final String imageUrl;
  final Color borderColor;
  final double borderWidth;

  const BorderAvatar({
    super.key,
    required this.radius,
    required this.imageUrl,
    required this.borderColor,
    required this.borderWidth,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: radius * 2,
      width: radius * 2,
      padding: EdgeInsets.all(borderWidth),
      decoration: BoxDecoration(
        border: Border.all(
          color: borderColor,
          width: borderWidth,
        ),
        borderRadius: BorderRadius.circular(radius),
        color: Colors.white,
      ),
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(radius),
            image: DecorationImage(
              image: CachedNetworkImageProvider(imageUrl),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}
