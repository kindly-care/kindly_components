import 'dart:io';

import 'package:flutter/material.dart';

class LocalBorderAvatar extends StatelessWidget {
  final double radius;
  final File imagePath;
  final Color borderColor;
  final double borderWidth;

  const LocalBorderAvatar({
    super.key,
    required this.radius,
    required this.imagePath,
    required this.borderColor,
    required this.borderWidth,
  }) ;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: radius * 2,
      width: radius * 2,
      padding: EdgeInsets.all(borderWidth),
      decoration: BoxDecoration(
        border: Border.all(
          color: borderColor,
          width: borderWidth,
        ),
        borderRadius: BorderRadius.circular(radius),
        color: Colors.white,
      ),
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(radius),
            image: DecorationImage(
              image: FileImage(imagePath),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}
