import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/src/auth/bloc/auth_bloc.dart';
import 'package:kindly_components/src/auth/repository/auth_repository.dart';
import 'package:kindly_components/src/exceptions/exceptions.dart';
import 'package:mocktail/mocktail.dart';

import '../../test_data.dart';

class MockAuthRepository extends Mock implements AuthRepository {}

void main() {
  late AuthRepository authRepository;

  setUp(() {
    authRepository = MockAuthRepository();
  });

  group('AuthBloc', () {
    test('initial state is AuthInitial', () {
      final authBloc = AuthBloc(authRepository: authRepository);
      expect(authBloc.state, AuthInitial());
      authBloc.close();
    });

    group('StartApp', () {
      blocTest<AuthBloc, AuthState>(
        'emits [Unauthenticated] when [StartApp] is added but [AppUser] is null',
        setUp: () {
          when(() => authRepository.fetchAuthDetails()).thenAnswer(
            (_) => Future.value(),
          );
          when(() => authRepository.fetchCurrentUser()).thenAnswer(
            (_) => Stream.value(null),
          );
        },
        build: () => AuthBloc(authRepository: authRepository),
        act: (bloc) => bloc.add(StartApp()),
        expect: () => <Matcher>[isA<Unauthenticated>()],
      );

      blocTest<AuthBloc, AuthState>(
        'emits [Authenticated] when [StartApp] is added and [AppUser] is not null',
        setUp: () {
          when(() => authRepository.fetchAuthDetails()).thenAnswer(
            (_) => Future.value(),
          );
          when(() => authRepository.fetchCurrentUser()).thenAnswer(
            (_) => Stream.value(user),
          );
        },
        build: () => AuthBloc(authRepository: authRepository),
        act: (bloc) => bloc.add(StartApp()),
        expect: () => <Matcher>[isA<Authenticated>()],
      );
    });

    group('SignUp', () {
      blocTest<AuthBloc, AuthState>(
        'emits [SigningUpError] when [SignUp] is added and '
        'registerWithEmail throws',
        setUp: () {
          when(() => authRepository.registerWithEmail(authDetails))
              .thenThrow(AuthException(errorMessage));
        },
        build: () => AuthBloc(authRepository: authRepository),
        act: (bloc) => bloc.add(const SignUp(details: authDetails)),
        expect: () =>
            <Matcher>[isA<SigningUpInProgress>(), isA<SigningUpError>()],
      );

      blocTest<AuthBloc, AuthState>(
        'emits [SigningUpSuccess] when [SignUp] is added and '
        'registerWithEmail is successfully called',
        setUp: () {
          when(() => authRepository.registerWithEmail(authDetails)).thenAnswer(
            (_) => Future.value(),
          );
        },
        build: () => AuthBloc(authRepository: authRepository),
        act: (bloc) => bloc.add(const SignUp(details: authDetails)),
        expect: () =>
            <Matcher>[isA<SigningUpInProgress>(), isA<SigningUpSuccess>()],
      );
    });

    group('Logout', () {
      blocTest<AuthBloc, AuthState>(
        'emits [Unauthenticated] when [Logout] is added and '
        'signOut is successfully called',
        setUp: () {
          when(() => authRepository.signOut()).thenAnswer(
            (_) => Future.value(),
          );
        },
        build: () => AuthBloc(authRepository: authRepository),
        act: (bloc) => bloc.add(Logout()),
        expect: () => <Matcher>[isA<Unauthenticated>()],
      );
    });

    group('ResetPassword', () {
      blocTest<AuthBloc, AuthState>(
        'emits [ResetPasswordError] when [ResetPassword] is added and '
        'resetPassword throws',
        setUp: () {
          when(() => authRepository.resetPassword(email))
              .thenThrow(AuthException(errorMessage));
        },
        build: () => AuthBloc(authRepository: authRepository),
        act: (bloc) => bloc.add(const ResetPassword(email: email)),
        expect: () => <Matcher>[
          isA<ResetPasswordInProgress>(),
          isA<ResetPasswordError>(),
        ],
      );

      blocTest<AuthBloc, AuthState>(
        'emits [ResetPasswordSuccess] when [ResetPassword] is added and '
        'resetPassword is successfully called',
        setUp: () {
          when(() => authRepository.resetPassword(email)).thenAnswer(
            (_) => Future.value(),
          );
        },
        build: () => AuthBloc(authRepository: authRepository),
        act: (bloc) => bloc.add(const ResetPassword(email: email)),
        expect: () => <Matcher>[
          isA<ResetPasswordInProgress>(),
          isA<ResetPasswordSuccess>(),
        ],
      );
    });

    group('DeleteAccount', () {
      blocTest<AuthBloc, AuthState>(
        'emits [Unauthenticated] when [DeleteAccount] is added and '
        'deleteAccount is successfully called',
        setUp: () {
          when(() => authRepository.deleteAccount()).thenAnswer(
            (_) => Future.value(),
          );
          when(() => authRepository.signOut()).thenAnswer(
            (_) => Future.value(),
          );
        },
        build: () => AuthBloc(authRepository: authRepository),
        act: (bloc) => bloc.add(Logout()),
        expect: () => <Matcher>[isA<Unauthenticated>()],
      );
    });
  });
}
