import 'package:kindly_components/src/models/models.dart';

const String email = 'test@email.com';
const AuthDetails authDetails = AuthDetails(email: email, password: 'password');
const String errorMessage = 'An error occurred.';
const String verificationMessage =
    "We've just send a verification link to your email inbox. You need to click on the link to verify your email address before you can sign into the app.";
const String resetPasswordMessage =
    "We've just send an email to $email. Please check your email inbox to reset your password.";

final user = AppUser(
  uid: 'uid',
  phone: '0',
  name: 'name',
  avatar: 'avatar.jpg',
  contacts: const <String>[],
  joinedDate: DateTime.now(),
  streamToken: 'streamToken',
  messageToken: 'messageToken',
);
